import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {AppLoading} from 'expo';
import {Root} from 'native-base';
import*as Font from 'expo-font';
import NewCustomer from './src/component/customer/new.customer/NewCustomer';

import {patch} from './src/patchs'; //Vì thẻ không thể nhận patch.Login
const Login = patch.Login;          // nên phải tạo 1 biến chứa component rồi mới nhập vào thẻ  

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import Reducers from './src/reducers/Reducers';
import PasswordRetrieval from './src/component/auth/password.retrieval/PasswordRetrieval';
import ManageAccount from './src/component/account/manage.account/ManageAccount';
import HeaderTabs from './src/component/header.tabs/HeaderTabs';
import MainStack from './routers';
import AddCustomers from './src/component/customer/add.customers/AddCustomers';
import ButtonLogIn from './src/component/auth/login/ButtonLogIn';
import InformationCustomer from './src/component/customer/information.customer/InformationCustomer';
import EditCustomer from './src/component/customer/edit.customer/EditCustomer';
import EditAccount from "./src/component/account/edit.account/EditAccount";

import MoneyTransfer from './src/component/banking.transactions/money.transfer/MoneyTransfer';
import HistoryTransaction from './src/component/banking.transactions/history.transaction/HistoryTransaction';
import CodeReferral from './src/component/banking.transactions/code.referral/CodeReferral';
import AddInformationBank from './src/component/banking.transactions/add.information.bank/AddInformationBank';
import ManagerAccountBank from './src/component/banking.transactions/manager.account.bank/ManagerAccountBank';
import HelpCustomer from './src/component/customer/help.customer/HelpCustomer';
import InformationAccount from './src/component/account/information.account/InformationAccount';
import AuthAccount from './src/component/account/auth.account/AuthAccount';
import WalletAccount from './src/component/banking.transactions/wallet.account/WalletAccount';
import SecurityAccount from './src/component/account/security.account/SecurityAccount';
import UpgradeWebsite from './src/component/account/upgrade.website/UpgradeWebsite';
import UpgradeEmail from './src/component/account/upgrade.email/UpgradeEmail';
import CompletedPay from './src/component/account/completed.pay/CompletedPay';
import ChooseBankPay from './src/component/account/choose.bank.pay/ChooseBankPay';
import AddCardAccount from './src/component/banking.transactions/add.card.account/AddCardAccount';
import AdminWeb from './src/component/administration/admin.web/Admin.Web';
import LisProduct from './src/component/administration/list.product/ListProduct';
import AllProduct from './src/component/administration/all.product/AllProduct';
import AllNews from './src/component/administration/all.news/AllNews';
import AddNewCategory from './src/component/administration/add.new.category/AddNewCategory';
import ImagePage from './src/component/administration/image.page/ImagePage';
import AllPopup from './src/component/administration/all.popup/AllPopup';
import ConfigWeb from './src/component/administration/config.web/ConfigWeb';

import createSagaMiddleware from 'redux-saga';
import rootSaga from './src/sagas/rootSagas';
import Registration from './src/component/auth/registration/Registration';

const sagaMiddleware = createSagaMiddleware()
    let store = createStore(Reducers, applyMiddleware(sagaMiddleware));
export default class App extends React.Component {

  state = {
    fontLoaded: true,
  };
  
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
    this.setState({ fontLoaded: false });
  }
  
  render() {
    if (this.state.fontLoaded) {
      return(
        <Root>
          <AppLoading/>
        </Root>
      );
    }
    
    
    return (
      <Provider store={store}>
        <Registration />
      </Provider>
    );    
  }
}
sagaMiddleware.run(rootSaga);
