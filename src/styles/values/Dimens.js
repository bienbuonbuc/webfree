import {Dimensions} from 'react-native';
export const dimension = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
}
export const padding = {
    xs: 5,  //Extra Small (rất nhỏ)
    sm: 10, //Small       (nhỏ)
    md: 15, //Medium      (trung bình)
    lg: 20, //Large       (lớn)
    xl: 25  //Extra Large (rất lớn)
}
export const sizeIcon = {
    xs: 5,
    sm: 10,
    md: 15,
    lg: 20,
    xl: 25
}
export const sizeText = {
    xs: 14,
    sm: 16,
    md: 18,
    lg: 20,
    xl: 22
}
export const margin = {
    xs: 5,
    sm: 10,
    md: 15,
    lg: 20,
    xl: 25

}
export const border = {
    xs: 5,
    sm: 10,
    md: 15,
    ld: 20,
    xl:25
}