export const Colors = {
    mainColor: "#0090FF",
    orange: "#FF9900",
    backgroundTextInput: '#C9C9C9',
    backgroundButton: '#DEDEDE',
    blueButton: '#168501'
}
