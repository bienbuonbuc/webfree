import {StyleSheet} from 'react-native';
import {Colors} from './values/Colors';
import *as Dimens from './values/Dimens';
import *as SizeText from './values/Dimens';

const Styles = StyleSheet.create({
    headerTab: {
        marginBottom: Dimens.margin.xs
    },
    titleTab: {
        backgroundColor: Colors.mainColor,
        flexDirection: 'row',
        height: 50,     //Dimens.dimension.fullHeight*0.054,
        justifyContent: 'center',
        
    },
    backIconHeader: {
        position: 'absolute',
        left: 10,
        top: 20,
    },
    titleHeader: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: SizeText.sizeText.md,
        marginTop: 20
    },
    textInput: {
        height: 35,
        paddingLeft: Dimens.padding.md,
        borderRadius: Dimens.border.xs,
        borderWidth: 0.7,
        borderColor: 'rgba(178, 188, 178, 1)',
        //borderColor: 'lightgrey',
        fontSize: Dimens.sizeText.sm,
        color: '#545353',
        backgroundColor: 'white',
        marginBottom: Dimens.margin.sm
        },
    buttonSmall: {
        paddingHorizontal: Dimens.padding.lg,
        paddingBottom: Dimens.padding.xs,
        paddingTop: Dimens.padding.xs,
        borderRadius: Dimens.border.xs,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
export default Styles;