import { connect } from 'react-redux';
import*as fetch from '../../../actions/actions';
import ManageAccount from '../../../component/account/manage.account/ManageAccount';

const mapStateToProps = (state) => {
    return {
        data : state.ManageAccountReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        renderManageAccount: (user) => {
            dispatch(fetch.inforAccountAction(user));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ManageAccount);