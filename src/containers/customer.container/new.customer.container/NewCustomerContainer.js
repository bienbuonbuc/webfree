import { connect } from 'react-redux';
import*as fetch from '../../../actions/actions';
import NewCustomer from '../../../component/customer/new.customer/NewCustomer';

const mapStateToProps = (state) => {
    return {
        data : state.NewCustomerReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        renderNewCustomer: (token, domain) => {
            dispatch(fetch.newCustomerAction(token, domain));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewCustomer);