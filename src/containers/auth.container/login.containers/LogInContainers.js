import { connect } from 'react-redux';
import ButtonLogIn from '../../../component/auth/login/ButtonLogIn';
import*as fetch from '../../../actions/actions';

const mapStateToProps = (state) => {
    return {
        data : state.LoginReducer,
        bien: 'saoooooooooo'
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickLogIn: (user) => {
            dispatch(fetch.loginAction(user));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ButtonLogIn);