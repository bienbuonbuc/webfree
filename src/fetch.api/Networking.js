const url = 'https://moma.vn/api/';
const qs = require('qs');
//Qs là query string, Truyền object vào r nó tự parse ra type là url qs
const header = {
    Accept: 'application/json',
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
};

export default class Http {
    static async get( patch ) {
        try {
            let response = await fetch( url + patch );
            let responseJson = await response.json();
            return responseJson;
        } catch(error) {
            console.log(error);
        }
    }
    
    static async post( patch, data ) {
        try {
            let response = await fetch( url + patch, {
                method: 'POST',
                headers: header,
                body: qs.stringify(data)
            })
            let responseJson = response.json();
            return responseJson;
        } catch(e) {
            console.log(e)
        }
    }
}