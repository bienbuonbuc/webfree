import Http from '../fetch.api/Networking';

//Trang Login
function* postUserLogin(user) {
    const data = {
        domain: user.domain,
        email: user.email,
        password: user.password
    }
    const response = yield Http.post('login', data);
    return response;
}

//Trang ManageAccount
function* getInforAccount(token) {
    const response = yield Http.get('info-user?token=' + token);
    return response;
}

//Trang NewCustomer
function* getNewCustomer(token, domain) {
    const response = yield Http.get('contact?token=' + token + '&domain=' + domain);
    return response;
}

//Trang InformationCustomer
function* getInformationCustomer(token, domain, contact_id) {
    const response = yield Http.get('contact/edit/' + contact_id + '?token=' + token + '&domain=' + domain);
    return response;
}

//Trang EditCustomer
function* postEditCustomer(infor) {
    const data = {
        token: infor.token,
        domain: infor.domain,
        name: infor.name,
        phone: infor.phone,
        email: infor.email,
        address: infor.address,
        message: infor.message
    }
    const response = yield Http.post('contact/update/' + infor.contact_id, data);
    return response;
}

export const Api ={
    postUserLogin,
    getInforAccount,
    getNewCustomer,
    getInformationCustomer,
    postEditCustomer
}