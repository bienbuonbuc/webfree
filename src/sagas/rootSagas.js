import { delay } from 'redux-saga';
import { call, all, fork } from 'redux-saga/effects';
import*as AllSaga from './AllSaga'

function* tasks() {
    yield fork(AllSaga.watchFetchUserLogin);
    yield fork(AllSaga.watchFetchInforAccount);
    yield fork(AllSaga.watchFetchNewCustomer);
    yield fork(AllSaga.watchFetchInformationCustomer);
    yield fork(AllSaga.watchFetchEditCustomer);
}
export default function* rootSaga() {
    yield call(tasks);
}