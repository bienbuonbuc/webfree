import*as TYPE from '../constans/action.type';
import { put, takeLatest } from 'redux-saga/effects';
import { Api } from './Api';
import { fetchLoginAction } from '../actions/actions';
import { Alert } from 'react-native';

//trang Login
function* fetchUserLogin(action) {
    try {
        const response = yield Api.postUserLogin(action.token);
        if(response.token)
        {
            yield put({ type: TYPE.FETCH_SUCCESS_LOGIN, token: response });
            return;
        }
        yield put({type: TYPE.FETCH_FAILED_LOGIN, error: response})
    } catch(e) {
        console.log('loi khong lây được token')
    }
}
export function* watchFetchUserLogin() {
    yield takeLatest(TYPE.LOGIN, fetchUserLogin)
}

//Trang ManageAccount
function* fetchInforAccount(action) {
    try {
        const response = yield Api.getInforAccount(action.token);
        if(response.result)
        {
            yield put({ type: TYPE.FETCH_SUCCESS_INFORACCOUNT, result: response.result });
            return;
        }
        console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh')
        yield put({ type: TYPE.FETCH_FAILED_INFORACCOUNT });
        
    }catch(e) {
        console.log('lỗi không lấy được thông tin user')
    }
}
export function* watchFetchInforAccount() {
    yield takeLatest(TYPE.INFORACCOUNT, fetchInforAccount)
}

//Trang NewCustomer
function* fetchNewCustomer(action) {
    try {
        const response = yield Api.getNewCustomer(action.token, action.domain);
        if(response.status == 200)
        {
            yield put({ type: TYPE.FETCH_SUCCESS_NEWCUSTOMER, content: response.content.data });
            return;
        }
        yield put({ type: TYPE.FETCH_FAILED_NEWCUSTOMER});
        
    }catch(e) {
        console.log('lỗi không lấy được thông tin user')
    }
}
export function* watchFetchNewCustomer() {
    yield takeLatest(TYPE.NEWCUSTOMER, fetchNewCustomer)
}

//Trang InformationCustomer
function* fetchInformationCustomer(action) {
    try {
        const response = yield Api.getInformationCustomer(action.token, action.domain, action.contact_id);
        if(response.status == 200)
        {
            yield put({ type: TYPE.FETCH_SUCCESS_IFORMATIONCUSTOMER, content: response.content[0] });
            return;
        }
        console.log('yyyyyyyyyyyyyyyyyy')
        yield put({ type: TYPE.FETCH_FAILED_IFORMATIONCUSTOMER});
    }catch(e) {
        console.log('lỗi không lấy được thông tin user')
    }
}
export function* watchFetchInformationCustomer() {
    yield takeLatest(TYPE.IFORMATIONCUSTOMER, fetchInformationCustomer)
}

//Trang EditCustomer
function* fetchEditCustomer(action) {
    try {
        const response = yield Api.postEditCustomer(action.infor);
        if(response.status == 200)
        {
            yield put({ type: TYPE.FETCH_SUCCESS_EDITCUSTOMER, content: response.content });
            Alert.alert(response.content)
            return;
        }
    }catch(e) {
        console.log('lỗi không lấy được thông tin user1111111111111')
    }
}
export function* watchFetchEditCustomer() {
    yield takeLatest(TYPE.EDITCUSTOMER, fetchEditCustomer)
}