
import Login from './component/auth/login/Login';
import  HeaderTabs from './component/header.tabs/HeaderTabs';
import  NewCustomer from './component/customer/new.customer/NewCustomer';
import  LoginContainers from './containers/auth.container/login.containers/LogInContainers';
import  styles from './styles/Styles';
import  Textinput from './component/text.input/Textinput';
export const patch = {
    Login: Login,
    HeaderTabs: HeaderTabs,
    NewCustomer: NewCustomer,
    LoginContainers: LoginContainers,
    styles: styles,
    Textinput: Textinput
}
