import *as Types from '../constans/action.type';

//trang Login
// export const loginSuccessAction = (response) => {
//     return {
//         type: Types.FETCH_SUCCESS_LOGIN,
//         response
//     }
// }
// export const loginFailedAction = (error) => {
//     return {
//         type: Types.FETCH_FAILED_LOGIN,
//         error
//     }
// }
export const loginAction = (token) => {
    return {
        type: Types.LOGIN,
        token
    }
}

//Trang ManageAccount
// export const inforAccountSuccessAction = (response) => {
//     return {
//         type: Types.FETCH_SUCCESS_INFORACCOUNT,
//         response
//     }
// }
// export const inforAccountFailedAction = (error='') => {
//     return {
//         type: Types.FETCH_FAILED_INFORACCOUNT,
//         error
//     }
// }
export const inforAccountAction = (token) => {
    return {
        type: Types.INFORACCOUNT,
        token
    }
}

//Trang NewCustomer
export const newCustomerAction = (token, domain) => {
    return {
        type: Types.NEWCUSTOMER,
        token,
        domain
    }
}

//Trang NewCustomer
export const informationCustomerAction = (token, domain, contact_id) => {
    return {
        type: Types.IFORMATIONCUSTOMER,
        token,
        domain,
        contact_id
    }
}

//Trang EditCustomer
export const editCustomerAction = (infor) => {
    return {
        type: Types.EDITCUSTOMER,
        infor
    }
}