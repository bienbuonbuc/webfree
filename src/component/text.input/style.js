import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
    text:{
    paddingLeft: 15,
    borderRadius: 8,
    borderWidth: 0.7,
    borderColor: 'rgba(178, 188, 178, 1)',
    //borderColor: 'lightgrey',
    fontSize: 15,
    color: '#545353',
    backgroundColor: 'white',
    marginBottom: 10
    }
})
export default styles;