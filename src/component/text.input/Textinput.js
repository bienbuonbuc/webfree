import React from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import styles from './style';

export default class Textinput extends React.Component{
    render(){
        return(
                <View >
                    <TextInput style={[styles.text, {height: this.props.height, width: this.props.width}]}
                    //Cái nào style tĩnh thì tách riêng, còn chứa tham số thì làm trực tiếp
                    //style nhận vào tham số là 1 object hoặc là 1 mảng các object
                    placeholder={this.props.placeholder}>
                    </TextInput>
                </View>
        );
    }
}
// const styles = StyleSheet.create({
//     text:{
//     height: this.props.height, //35,
//     width: this.props.width,
//     paddingLeft: 15,
//     borderRadius: 8,
//     borderWidth: 0.7,
//     borderColor: 'rgba(178, 188, 178, 1)',
//     //borderColor: 'lightgrey',
//     fontSize: 15,
//     color: '#545353',
//     backgroundColor: 'white',
//     marginBottom: 10
//     }
// })