import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, AsyncStorage} from 'react-native';
const { width: WIDTH } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import star from '../../../../assets/image/icon/star.jpg';
import security from '../../../../assets/image/icon/shield.jpg';
import update from '../../../../assets/image/icon/update.jpg';
import code from '../../../../assets/image/icon/code.jpg';
import email from '../../../../assets/image/icon/email.jpg';
import phone from '../../../../assets/image/icon/phone.jpg';
import styles from './styles';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from "../../../styles/Styles";

let dataManage = [
    {
        image: code,
        text: 'Mã giới thiệu',
        screen: 'CodeReferral'
    },
    {
        image: email,
        text: 'Nâng cấp Email marketing',
        screen: 'UpgradeEmail'
    },
    {
        image: update,
        text: 'Nâng cấp Website bán hàng',
        screen: 'UpgradeWebsite'
    },
    {
        image: security,
        text: 'Chính sách bảo mật',
        screen: 'SecurityAccount'
    },
    {
        image: star,
        text: 'Đánh giá ứng dụng',
        screen: ''
    },
    {
        image: phone,
        text: 'Hỗ trợ chăm sóc khách hàng',
        screen: 'HelpCustomer'
    },
]
export default class ManageAccount extends Component {
    renderItem = (dataSource) => {
        return dataSource.map((Element,index) => {
            return(
                <TouchableOpacity onPress={() => this.props.navigation.navigate(Element.screen)} key={index} style = {styles.menuSupport}>
                    <Image style = {styles.icon} source = {Element.image} />
                    <Text>{Element.text}</Text>
                </TouchableOpacity>
            );
        })
    }
    async componentDidMount() {
        const token = await AsyncStorage.getItem('token')
        this.props.renderManageAccount(token);
    }
    render() {
        // console.log('propppppppppp',this.props.data)
        return (
            <View>
                <HeaderTabs title={"THÔNG TIN TÀI KHOẢN"} hidden={false} ></HeaderTabs>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('EditAccount')} style={styles.informationAccount}>
                    <View style={styles.accountView}>
                        <Image resizeMode={'contain'} style={styles.imageAccount} source={require('../../../../assets/image/people.png')} />
                    </View>
                    <View style={styles.textInformation}>
                        <Text style={{fontWeight: 'bold'}}>{this.props.data.name}</Text>
                        <Text>{this.props.data.user_email}</Text>
                        <Text>{this.props.data.phone}</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.container}>
                    <View style={styles.notification}>
                        <Text style ={styles.textNotification}>Tài khoản của bạn hết hạn sau 356 ngày</Text>
                    </View>
                    {this.renderItem(dataManage)}
                    <View style={{alignItems:"center", marginTop: 10}}>
                        <TouchableOpacity style={[Styles.buttonSmall, styles.logoutBtn,]}>
                        {/* khi truyền vào mảng các style thì mảng nào truyền vào sau sẽ được ưu tiên */}
                            <Text style ={styles.textLogout}>Đăng xuất</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
