import {border} from "../../../styles/values/Dimens";

const { width: WIDTH } = Dimensions.get('window');
const { height : HEIGHT} = Dimensions.get('window');
import {StyleSheet, Platform, Dimensions} from 'react-native';
import *as Color from '../../../styles/values/Colors';
import *as Dimens from '../../../styles/values/Dimens';

const styles = StyleSheet.create({
container: {
    backgroundColor: '#fff',
    },
menuSupport : {
    flexDirection:"row",
    alignItems:"center", 
    paddingLeft: Dimens.padding.sm,
    paddingBottom : Dimens.padding.sm,
    paddingTop :Dimens.padding.sm,
    borderBottomWidth : 0.5,
    borderBottomColor : "gray",
    backgroundColor : "white"
},
logoutBtn: {
    backgroundColor: Color.Colors.orange,
    borderRadius: border.md,
    width: 160
},
icon: {
    width : 20,
    height : 20,
    resizeMode:"contain",
    marginRight: Dimens.margin.lg
},
notification: {
    backgroundColor:"#ea983e",
    marginTop: Dimens.margin.sm
},
textNotification:{
    textAlign:"center",
    color :"white",
    paddingBottom: Dimens.padding.xs,
    paddingTop: Dimens.padding.xs
},
textLogout: {
    color: 'white',
    fontSize: Dimens.sizeText.md
},
informationAccount: {
    flexDirection: 'row',
    height: HEIGHT*0.08,
    alignItems: 'center',
    marginTop: Dimens.margin.xs
},
imageAccount: {
    height:Platform.OS==='ios' ? 55 : HEIGHT*0.08,
    width: Platform.OS==='ios' ? 55 : HEIGHT*0.08,
    borderRadius: Platform.OS==='ios' ? 28 : 200,
    marginLeft: Dimens.margin.sm
},
textInformation: {
    marginLeft: Dimens.margin.sm
},
accountView: {
    borderRadius: Platform.OS==='ios' ? 28 : 200,
}
});
export default styles;