import React, {Component} from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import { Colors } from '../../../styles/values/Colors';
import { padding, border, sizeText, margin, dimension } from '../../../styles/values/Dimens';
import Modal from 'react-native-modalbox';

export default class InformationAccount extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isModalVisible: false
        }
    }
    
      toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };
    render(){
        return(
            <View style={styles.container}>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"THÔNG TIN CÁ NHÂN"}></HeaderTabs>
                <View style={styles.item}>
                    <Text style={styles.titleItem}>Xác thực tài khoản</Text>
                    <Text>Tài khoản được xác thực sẽ có mức độ bảo mật cao hơn</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AuthAccount')}
                        style={[Styles.buttonSmall, styles.buttonSmall]}>
                        <Text style={{color:'white'}}>Xác thực</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.item}>
                    <Text style={styles.titleItem}>Xác thực địa chỉ email</Text>
                    <Text>bienbuonbuc@gmail.com</Text>
                    <TouchableOpacity onPress={() => this.toggleModal()} style={[Styles.buttonSmall, styles.buttonSmall]}>
                        <Text style={{color:'white'}}>Xác thực</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.item}>
                    <Text style={styles.titleItem}>Cập nhật thông tin cá nhân</Text>
                    <Text>Tài khoản được xác thực sẽ có mức độ bảo mật cao hơn</Text>
                    <TouchableOpacity style={[Styles.buttonSmall, styles.buttonSmall]}>
                        <Text style={{color:'white'}}>Cập nhật</Text>
                    </TouchableOpacity>
                </View>
                <Modal style={styles.modal}
                    isOpen={this.state.isModalVisible}
                    position={'center'}
                    backdropOpacity={0.5}
                >
                    <Text style={{fontWeight:'bold'}}>Đã gửi Email xác thực</Text>
                    <Text style={{marginTop:5}}>
                        Một email xác thực đã được gửi đến địa chỉ bienbuonbuc@gmail.com. Quý khách
                        vui lòng mở email và bấm "Xác nhận" để hoàn thành việc xác thực email.
                    </Text>
                    <View style={{alignItems: 'flex-end'}}>
                        <TouchableOpacity onPress={() => this.toggleModal()} style={[Styles.buttonSmall, {backgroundColor: '#989897', marginTop:20}]}>
                            <Text>Đóng</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    item: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        marginHorizontal: 15,
        marginTop: 15,
        paddingTop: padding.sm,
        paddingBottom: padding.sm,
        borderRadius: border.xs,
        borderColor: Colors.backgroundTextInput,
        paddingHorizontal: padding.xs
    },
    titleItem: {
        fontWeight: 'bold',
        fontSize: sizeText.sm,
    },
    buttonSmall: {
        backgroundColor: '#989897',
        marginTop: margin.xs
    },
    modal: {
        height: dimension.fullHeight*0.25,
        borderRadius: border.sm,
        padding: padding.sm,
        width: dimension.fullWidth*0.85
    }
})