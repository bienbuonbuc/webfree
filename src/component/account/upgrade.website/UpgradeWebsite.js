import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import ItemUpgradeWebsite from './ItemUpgradeWebsite';

export default class UpgradeWebsite extends Component{
    render(){
        return(
            <View style={{backgroundColor:'#DEDEDE'}}>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"NÂNG CẤP WEBSITE BÁN HÀNG"}/>
                <ScrollView style={{paddingHorizontal:10, marginTop:-5}}>
                    <ItemUpgradeWebsite package={"GÓI VIP 1"} donate={"Tiết kiệm 0 đồng"} 
                        money={"2.400.000 VNĐ"} time={"01 năm"} button={"NÂNG CẤP"} backColor={'#6E00FF'} />
                    <ItemUpgradeWebsite package={"GÓI VIP 2"} donate={"Mua 2 năm tặng 1 năm"} 
                        money={"4.800.000 VNĐ"} time={"03 năm"} button={"NÂNG CẤP"} backColor={'#FF9100'} />
                    <ItemUpgradeWebsite package={"GÓI VIP 3"} donate={"Mua 3 năm tặng 2 năm"} 
                        money={"7.200.000 VNĐ"} time={"05 năm"} button={"NÂNG CẤP"} backColor={'#108D10'} />
                    <ItemUpgradeWebsite package={"GÓI ĐẶC BIỆT"} donate={"Dành cho doanh nghiệp cỡ lỡn"} 
                        money={"Liên hệ"} time={"Thỏa thuận"} button={"Liên hệ: 097 456 1736"} backColor={'#FF0000'} />
                </ScrollView>
            </View>
        );
    }
}