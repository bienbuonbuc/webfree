import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { dimension, sizeIcon, sizeText, padding, border, margin } from '../../../styles/values/Dimens';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Styles from '../../../styles/Styles';

export default class ItemUpgradeWebsite extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={[styles.title,{backgroundColor: this.props.backColor}]}>
                    <Text style={[styles.packageText,{marginBottom:0}]}>{this.props.package}</Text>
                    <Text style={{color:'white'}}>( {this.props.donate} )</Text>
                    <Text style={[styles.packageText,{fontSize:14, marginTop:0}]}>{this.props.money }</Text>
                </View>
                <Icon style={styles.icon} name={"ios-star"} color={"white"} size={30} />

                <View style={[styles.infor,{paddingTop:0}]}>
                    <View style={styles.inforTime}>
                        <Text>Thời gian sử dụng</Text>
                        <Text style={styles.limit}>{this.props.time}</Text>
                    </View>
                    <Text style={styles.inforText}>Đăng kí website lên các công cụ tìm kiếm</Text>
                    <Text style={styles.inforText}>Hỗ trợ mọi màn hình Smart phone và Máy tính bảng</Text>
                    <Text style={styles.inforText}>Website 1 ngôn ngữ</Text>
                    <Text style={styles.inforText}>KHởi tạo, tích hợp Google Analytic</Text>
                    <Text style={styles.inforText}>Chuẩn SEO, đầy đủ chức năng hỗ trợ SEO</Text>
                    <Text style={styles.inforText}>Tích hợp Fanpage, Google map, Gọi điện</Text>
                    <Text style={styles.inforText}>Tích hợp công cụ bán hàng đa kênh</Text>
                    <Text style={styles.inforText}>Tích hợp công cụ phân tích đối thủ</Text>
                    <Text style={styles.inforText}>Tích hợp quản lý vận chuyển</Text>
                    <Text style={styles.inforText}>Tích hợp công cụ quản lý Mạng xã hội</Text>
                    <View style={styles.inforTime}>
                        <Text>Số lượng sản phẩm đăng tải</Text>
                        <Text style={[styles.limit,{backgroundColor:'#00ACA7'}]}>Không giới hạn</Text>
                    </View>
                    <View style={styles.inforTime}>
                        <Text>Số lượng tin tức đăng tải</Text>
                        <Text style={[styles.limit,{backgroundColor:'#00ACA7'}]}>Không giới hạn</Text>
                    </View>
                    <View style={styles.inforTime}>
                        <Text>Số lượng khách hàng</Text>
                        <Text style={[styles.limit,{backgroundColor:'#00ACA7'}]}>Không giới hạn</Text>
                    </View>
                    <View style={styles.inforTime}>
                        <Text>Thay đổi tên miền</Text>
                        <Text style={[styles.limit,{backgroundColor:'#05A200'}]}>Miễn phí</Text>
                    </View>
                    <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor:'#01C522', marginTop:10}]}>
                        <Text style={{color:'white', fontWeight:'bold'}}>{this.props.button}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        height: dimension.fullHeight-65,
        backgroundColor: 'white',
        marginTop: 20
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        position: 'absolute',
        top: 5,
        left:5
    },
    packageText: {
       fontWeight:'bold',
       color: 'white',
       fontSize: sizeText.md,
       marginTop: 5,
       marginBottom: 5
    },
    infor: {
        padding: padding.sm
    },
    inforTime: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: margin.sm
    },
    limit: {
        backgroundColor: '#FC7539',
        paddingHorizontal: 10,
        borderRadius: border.xs,
        color: 'white'
    },
    inforText: {
        marginTop: margin.sm
    },
});