import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { padding, dimension, border, margin } from '../../../styles/values/Dimens';

export default class CompletedPay extends Component {
    render() {
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"HOÀN TẤT THANH TOÁN"} />
                <View style={styles.container}>
                    <Text>Để hoàn tất thủ tục thanh toán mua thêm thời gian sử dụng</Text>
                    <Text>
                    {'\t'}{'\t'}Để đơn hàng được khởi tạo, quý khách vui lòng thực hiện việc chuyển khoản tổng tiền là
                    <Text style={{color:'#FF7700', fontSize:16}}> 4,800,000</Text> vào một trong những tài khoản ngân hàng hiển thị ngay bên dưới.
                    </Text>
                    <Text>
                    {'\t'}{'\t'}Trong nội dung chuyển khoản quý khách vui lòng điền mã đơn hàng là <Text style={{color:'red', fontSize:16}}>VN3C-MT36-263</Text>
                    </Text>
                    <Text>
                    {'\t'}{'\t'}Nếu quý khách không thể ghi mã đơn hàng vào nội dung chuyển khoản,
                        quy khách vui lòng chụp hình ủy nhiệm chi và gửi vào mail: vn3ctran@gmail.com
                        với tựa đề: Ủy nhiệm đơn hàng <Text style={{color:'#FF7700', fontSize:16}}>VN3C-MT36-263</Text>
                    </Text>
                    <View style={styles.bank}>
                        <Image style={styles.image} resizeMode={'stretch'} source={require('../../../../assets/image/bank/BIDV.jpg')} />
                        <View>
                            <Text>Ngân hàng BIDV - Chi nhánh Thanh Xuân</Text>
                            <Text>2221.0003.8490.14</Text>
                            <Text>Nội dung chuyển khoản: <Text style={{color:'red', fontSize:16}}>VN3C-MT36-263</Text></Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    } 
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: padding.md,
        backgroundColor: '#DEDEDE',
        marginTop: -5,
        paddingTop: padding.sm,
        height: dimension.fullHeight
    },
    bank: {
        flexDirection: 'row',
        padding: padding.sm,
        height: 80,
        backgroundColor: 'white',
        marginTop: margin.sm
    },
    image: {
        width:80,
        height: null,
        borderRadius: border.sm,
        marginRight: margin.sm
    },
});