import {border, margin, padding} from "../../../styles/values/Dimens";

const { width: WIDTH } = Dimensions.get('window');
const { height : HEIGHT} = Dimensions.get('window');
import {StyleSheet, Platform, Dimensions} from 'react-native';
import *as Color from '../../../styles/values/Colors';
import *as Dimens from '../../../styles/values/Dimens';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    informationAccount: {
        flexDirection: 'row',
        height: HEIGHT*0.08,
        width: Dimens.dimension.fullWidth,
        alignItems: 'center',
        marginTop: Dimens.margin.xs
    },
    imageAccount: {
        height: Platform.OS==='ios' ? 65 : HEIGHT*0.08,
        width: Platform.OS==='ios' ? 65 : HEIGHT*0.08,
        borderRadius: Platform.OS==='ios' ? 32 : 200,
        marginLeft: Dimens.margin.sm
    },
    editImage: {
        marginLeft: Dimens.margin.sm,
        backgroundColor: Color.Colors.backgroundButton,
        borderRadius: border.xs,
        padding: padding.xs
    },
    accountView: {
        borderRadius: Platform.OS==='ios' ? 32 : 200,
    },
    informationForm: {
        padding: padding.sm,
    },
    nameText: {
        marginTop: margin.xs
    },
    choosePassWord: {
        marginTop: margin.xs,
        flexDirection: 'row',
        alignItems: 'center'
    },
    choose: {
        height: 20,
        width: 20,
        backgroundColor: Color.Colors.backgroundButton,
        marginRight: margin.sm
    },
    saveInformation: {
        backgroundColor: Color.Colors.orange,
        borderRadius: border.md,
        paddingTop: 5,
        paddingBottom: 5
    },
    textSave: {
        color: 'white',
        fontSize: Dimens.sizeText.md
    }
});
export default styles;