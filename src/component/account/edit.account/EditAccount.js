import React, {Component} from 'react';
import {Image, Text, View, TouchableOpacity, TextInput} from "react-native";
import HeaderTabs from "../../header.tabs/HeaderTabs";
import styles from "./styles";
import Styles from "../../../styles/Styles";

export default class EditAccount extends Component{
    render() {
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"CHỈNH SỬA THÔNG TIN"} />
                <View style={styles.informationAccount}>
                    <View style={styles.accountView}>
                        <Image resizeMode={'contain'} style={styles.imageAccount} source={require('../../../../assets/image/people.png')} />
                    </View>
                    <TouchableOpacity style={styles.editImage}>
                        <Text>thay ảnh đại diện</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.informationForm}>
                    <Text style={styles.nameText}>Tài khoản Email</Text>
                    <TextInput style={[ Styles.textInput, {marginTop: 5} ]}
                               placeholder = {"Tài khoản Email"}
                    />
                    <Text style={styles.nameText}>Họ và tên</Text>
                    <TextInput style={[ Styles.textInput, {marginTop: 5} ]}
                               placeholder = {"Họ và tên"}
                    />
                    <Text style={styles.nameText}>Số điện thoại</Text>
                    <TextInput style={[ Styles.textInput, {marginTop: 5} ]}
                               placeholder = {"Số điện thoại"}
                    />

                    <View style={styles.choosePassWord}>
                        <View style={styles.choose}></View>
                        <Text>Chọn nếu muốn thay đổi mật khẩu</Text>
                    </View>
                    <TextInput style={[ Styles.textInput, {marginTop: 10} ]}
                               placeholder = {"Thay đổi mật khẩu"}
                    />
                </View>
                <View style={{alignItems:"center"}}>
                    {/*Nếu không có alignItems thì thằng Touchble sẽ hiểu là cả thẻ view là nút bấm*/}
                    {/*Nên phải cho thêm align vào để xác định chữ ở giữa, rồi nó mới xác định lại*/}
                    {/*chiều dài rộng của nút bấm theo padding ở Styles.buttonSmall*/}
                    <TouchableOpacity style={[Styles.buttonSmall, styles.saveInformation]}>
                        <Text style ={styles.textSave}>Lưu thay đổi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}