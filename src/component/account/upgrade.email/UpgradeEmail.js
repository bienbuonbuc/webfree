import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, TextInput } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { padding, dimension, sizeText, margin, border } from '../../../styles/values/Dimens';
import Icon from 'react-native-vector-icons/Ionicons';
import Styles from '../../../styles/Styles';
import Modal from 'react-native-modalbox';
import { Colors } from '../../../styles/values/Colors';

export default class UpgradeEmail extends Component{
    constructor(props){
        super(props);
        this.state = {
            isModalVisible: false
        }
    }
    
      toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };
    render(){
        return(
            <View style={{backgroundColor:'#DEDEDE', flex:1}}>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"NÂNG CẤP EMAIL MARKETING"} />
                <View style={styles.container}>
                    <View style={[styles.title,{backgroundColor:'#FF0099'}]}>
                            <Text style={[styles.packageText,{marginBottom:0}]}>GÓI EMAIL 1</Text>
                            <Text style={[styles.packageText,{fontSize:14, marginTop:0}]}>2.400.000 VNĐ</Text>
                            <Icon style={styles.icon} name={"ios-star"} color={"white"} size={30} /> 
                            {/* phải dùng Icon ở đây thì position mới chèn vào thẻ cha nếu dùng ở ngoài thì sẽ chèn lên View to nhất */}
                    </View>
                    <View style={styles.detail}>        
                        <View style={styles.inforTime}>
                            <Text>Thời gian sử dụng</Text>
                            <Text style={styles.limit}>01 năm</Text>
                        </View>
                        <View style={styles.inforTime}>
                            <Text>Số lượng Email/tháng</Text>
                            <Text style={[styles.limit,{backgroundColor:'#05A200'}]}>5.000 email</Text>
                        </View>
                        <Text style={styles.inforText}>Cho phép gửi email tới khách hàng</Text>
                        <Text style={styles.inforText}>Cho phép đặt lịch gửi mail chăm sóc khách hàng</Text>
                        <TouchableOpacity onPress={() => this.toggleModal()}
                            style={[Styles.buttonSmall,{backgroundColor:'#01C522', marginTop:10}]}>
                            <Text style={{color:'white', fontWeight:'bold'}}>NÂNG CẤP</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={[styles.container,{marginTop:15}]}>
                    <View style={[styles.title,{backgroundColor:'#B1009F'}]}>
                            <Text style={[styles.packageText,{marginBottom:0}]}>GÓI EMAIL 2</Text>
                            <Text style={[styles.packageText,{fontSize:14, marginTop:0}]}>6.000.000 VNĐ</Text>
                            <Icon style={styles.icon} name={"ios-star"} color={"white"} size={30} /> 
                            {/* phải dùng Icon ở đây thì position mới chèn vào thẻ cha nếu dùng ở ngoài thì sẽ chèn lên View to nhất */}
                    </View>
                    <View style={styles.detail}>        
                        <View style={styles.inforTime}>
                            <Text>Thời gian sử dụng</Text>
                            <Text style={styles.limit}>01 năm</Text>
                        </View>
                        <View style={styles.inforTime}>
                            <Text>Số lượng Email/tháng</Text>
                            <Text style={[styles.limit,{backgroundColor:'#05A200'}]}>20.000 email</Text>
                        </View>
                        <Text style={styles.inforText}>Cho phép gửi email tới khách hàng</Text>
                        <Text style={styles.inforText}>Cho phép đặt lịch gửi mail chăm sóc khách hàng</Text>
                        <TouchableOpacity onPress={() => this.toggleModal()}
                            style={[Styles.buttonSmall,{backgroundColor:'#01C522', marginTop:10}]}>
                            <Text style={{color:'white', fontWeight:'bold'}}>NÂNG CẤP</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal style={styles.modal}
                    isOpen={this.state.isModalVisible}
                    position={'center'}
                    backdropOpacity={0.5}
                >
                    <View style={styles.viewTitleModal}>
                        <Text style={styles.titleModal}>Nâng cấp gói Email 1 (5000 mail/tháng)</Text>
                        <TouchableOpacity onPress={() => this.toggleModal()} style={[Styles.buttonSmall]}>
                            <Text style={{fontWeight: 'bold',marginTop:-10,marginRight:-10,fontSize:16}}>X</Text>
                        </TouchableOpacity>
                    </View>
                    <Text>Số điện thoại xác nhận thanh toán</Text>
                    <TextInput placeholder="Số điện thoại" style={[Styles.textInput,{marginTop:15}]} />
                    <View style={{alignItems:'flex-start'}}>
                        <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor:'#017EA5'}]}>
                            <Text style={{color:'white'}}>Thanh toán</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{borderBottomWidth:1, alignItems:'center', marginTop:5}}>
                        <Text style={{marginBottom:-10, backgroundColor:'white',  width:60, textAlign:'center'}}>Hoặc</Text>
                    </View>
                    <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor:'#01C522',marginTop:15}]}>
                        <Text style={{color:'white'}}>Thanh toán qua Ví điện tử VNPAY</Text>
                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: padding.sm,
        marginTop: margin.xs
    },
    detail: {
        paddingHorizontal: padding.sm,
        height: dimension.fullHeight*0.3,
        backgroundColor: 'white',
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    packageText: {
        fontWeight:'bold',
        color: 'white',
        fontSize: sizeText.md,
        marginTop: 5,
        marginBottom: 5
     },
     icon: {
        position: 'absolute',
        top: 5,
        left:5
    },
    inforTime: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: margin.sm
    },
    limit: {
        backgroundColor: '#FC7539',
        paddingHorizontal: 10,
        borderRadius: border.xs,
        color: 'white'
    },
    inforText: {
        marginTop: margin.sm
    },
    modal: {
        height: dimension.fullHeight*0.38,
        borderRadius: border.sm,
        padding: padding.sm,
        width: dimension.fullWidth*0.85
    },
    viewTitleModal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        paddingBottom: padding.xs,
        marginBottom: 10,
    },
    titleModal: {
        color: Colors.mainColor,
        fontSize: sizeText.sm
    }
});