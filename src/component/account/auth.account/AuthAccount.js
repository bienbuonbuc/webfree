import React, {Component} from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { padding, margin, sizeText, dimension, border } from '../../../styles/values/Dimens';

export default class AuthAccount extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"XÁC THỰC TÀI KHOẢN"}></HeaderTabs>
                <View style={styles.container}>
                    <Text style={[styles.textName, {marginTop:-5}]}>
                        Họ và tên <Text style={{color:'red'}}>*</Text>
                    </Text>
                    <TextInput style={[Styles.textInput, styles.textInput]} placeholder={"Nhập họ và tên"} />
                    <Text style={styles.textName}>Số CMND/CCCD <Text style={{color:'red'}}>*</Text></Text>
                    <TextInput style={[Styles.textInput, styles.textInput]} placeholder={"Nhập số CMND/CCCD"} />
                    <Text style={styles.textName}>ẢNH CHỤP 2 MẶT CMND/CCCD</Text>
                    <View style={styles.camera}>
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.textCamera}>mặt trước</Text>
                            <Icon name={"ios-camera"} size={100} color={'#707070'}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.button, {marginLeft:dimension.fullWidth*0.0226}]}>
                            <Text style={styles.textCamera}>mặt sau</Text>
                            <Icon name={"ios-camera"} size={100} color={'#707070'}></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        padding: padding.sm,
    },
    textName: {
        marginBottom: margin.xs,
        fontSize: sizeText.sm
    },
    button: {
        borderWidth: 0.8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: dimension.fullWidth*0.4661,
        height: 80         ,
        borderRadius: border.xs
    },
    camera: {
        flexDirection: 'row',
    },
    textCamera: {
        marginRight: margin.xs,
        fontSize: sizeText.sm
    }
})