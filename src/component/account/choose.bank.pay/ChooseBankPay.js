import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity, TextInput } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import BIDV from '../../../../assets/image/bank/BIDV.jpg';
import Ag from '../../../../assets/image/bank/Agribank.png';
import { dimension, margin, sizeText, padding } from '../../../styles/values/Dimens';
import Styles from '../../../styles/Styles';

dataRender = [
    {1:1},{1:1},{1:1},{1:1},{1:1},
    {1:1}
]
export default class ChooseBankPay extends Component {
    renderBanks = (data) => {
        return data.map((Element,index) => {
            return (
                <View key={index} style={styles.item}>
                    <TouchableOpacity style={styles.button}>
                        <Image resizeMode={'contain'} style={styles.image} source={BIDV} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>
                        <Image resizeMode={'contain'} style={styles.image} source={Ag} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>
                        <Image resizeMode={'contain'} style={styles.image} source={BIDV} />
                    </TouchableOpacity>
                </View>
            );
           })
    }
    render() {
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"HOÀN TẤT THANH TOÁN"} />
                <Text style={styles.text}>Chọn ngân hàng thanh toán</Text>
                <View style={styles.container}>
                    {this.renderBanks(dataRender)}
                </View>
                <View style={{paddingHorizontal:10}}>
                    <Text style={[styles.text,{marginLeft:0, marginBottom:5, marginTop:10}]}>Nhập số tiền cần thanh toán</Text>
                    <TextInput placeholder="Nhập số tối thiểu 100.000 VNĐ" style={Styles.textInput} />
                    <View style={{alignItems:'flex-start'}}>
                        <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor:'#196303'}]}>
                            <Text style={{color:'white', fontSize:16}}>Thanh toán</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        width: dimension.fullWidth,
        alignItems: 'stretch'
    },
    text: {
        marginLeft: margin.sm,
        fontSize: sizeText.md,
        fontWeight: 'bold',
        marginTop: 5
    },
    item: {
        
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: padding.lg,
        marginTop: margin.sm
    },
    button: {
        borderWidth: 1,
        height: 60,
        width: dimension.fullWidth*0.26,
        padding: padding.xs,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: 50
    },
});