import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import { margin, padding } from '../../../styles/values/Dimens';

dataItem = [
    {a:'a'},
    {a:'a'},
    {a:'a'},
    {a:'a'},
    {a:'a'},
    {a:'a'},
    {a:'a'},
]
export default class AllProduct extends Component{
    renderItem = (data) => {
        return data.map((Element,index) =>{
            return(
                <View style={styles.item} key={index}>
                    <View style={{flex:1}}>
                        <View style={styles.id}>
                            <Text style={{flex:1}}>ID: 236</Text>
                            <Text style={{flex:2}}>Mã sản phẩm: CNCY</Text>
                        </View>
                        <Text>Tên: <Text style={styles.name}>Quần âu cá tính</Text></Text>
                        <View style={styles.view}>
                            <Text style={{flex:1}}>Giá: <Text style={styles.money}>200.000</Text></Text>
                            <Text style={{flex:2}}>Lượt xem: 668</Text>
                        </View>
                    </View>
                    <Image resizeMode={'stretch'} style={styles.image} source={require('../../../../assets/image/bank/Shinhan.jpg')}></Image>
                </View>
            );
        });
    }
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"TẤT CẢ SẢN PHẨM"}/>
                <View style={styles.buttonSmall}>
                    <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor: '#007AB1'}]}>
                        <Text style={{color:'white'}}>Thêm mới</Text>
                    </TouchableOpacity>
                </View>
                {this.renderItem(dataItem)}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    buttonSmall: {
        alignItems: 'flex-start',
        marginLeft: margin.md,
        marginTop: margin.xs
    },
    item: {
        marginTop: margin.sm,
        flexDirection: 'row',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        paddingHorizontal: padding.md,

    },
    image: {
        height: 75,
        width: 60,
        marginBottom: margin.xs
    },
    id: {
        flexDirection: 'row',
        marginBottom: margin.xs 
    },
    view: {
        flexDirection: 'row',
        marginTop: margin.xs
    },
    name: {
        color: '#0D6EFF'
    },
    money: {
        color: 'red'
    }
});