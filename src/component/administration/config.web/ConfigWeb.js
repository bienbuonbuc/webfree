import React, { Component } from 'react';
import { View } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';

export default class ConfigWeb extends Component {
    render() {
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"CẤU HÌNH WEBSITE"} />
            </View>
        );
    } 
}