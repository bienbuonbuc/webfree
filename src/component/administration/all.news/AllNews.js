import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import { margin, padding, sizeIcon, sizeText } from '../../../styles/values/Dimens';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import Icon from 'react-native-vector-icons/Feather';

export default class AllNews extends Component{
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['ID', 'Tiêu đề', 'Thao tác','Thao tác'],
            tableData: [
                
            ]
        }
    }
    rederItem = (data) => {
        return data.map((Element, index) => {
            Element.push(
                <View style={styles.buttonTable}>
                    <TouchableOpacity>
                        <Text style={{color:'#4582FC'}}>link</Text>
                    </TouchableOpacity>
                </View>
            )
            Element.push(
                <View style={styles.buttonTable}>
                    <TouchableOpacity style={styles.editButton} >
                        <Icon name={"edit-2"} color={'white'} size={sizeIcon.md} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.deleteButton} >
                        <Icon name={"trash-2"} color={'white'} size={sizeIcon.md} />
                    </TouchableOpacity>
                </View>
            )
            return(
                <TableWrapper style={{flexDirection:'row'}} key={index}>
                    <Rows flexArr={[2, 7, 3, 3]} data={[Element]} textStyle={styles.text}/>
                </TableWrapper>
                //Element là các phần tử trong mảng, phải đóng [] để map() hiểu là 1 phần tử

                // <Rows key={index} data={[Element,[<Text>aaa</Text>]]} textStyle={styles.text}/>
            );
        });
    }

    render(){
        const dataItem = [
            ['325', 'Quần áo nam'],
            ['324', 'Quần áo nữ'],
            ['323', 'Quần áo'],
            ['322', 'Váy trẻ nhỏ'],
            ['321', 'Váy xinh'],
            ['320', 'Phụ kiện'],
        ]
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"TẤT CẢ TIN TỨC"}/>
                <View style={styles.buttonSmall}>
                    <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor: '#007AB1'}]}>
                        <Text style={{color:'white'}}>Thêm mới</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <Table borderStyle={{borderWidth: 1, borderColor: '#c8e1ff'}}>
                        <Row data={this.state.tableHead} flexArr={[2, 7, 3, 3]} style={styles.head} textStyle={[styles.text,{fontSize: sizeText.sm}]}/>
                        {this.rederItem(dataItem)}
                    </Table>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: padding.md,
        paddingTop: padding.sm,
        backgroundColor: '#fff',
        height:350,
    },
    head: {
        height: 40,
        backgroundColor: '#f1f8ff',
    },
    text: {
        margin: padding.xs,
        textAlign: 'center',
    },
    buttonSmall: {
        alignItems: 'flex-start',
        marginLeft: margin.md,
        marginTop: margin.xs
    },
    buttonTable: {
        flexDirection: 'row',
        padding: padding.xs,
        justifyContent: 'center',
        alignItems: 'center'
    },
    editButton: {
        backgroundColor: '#4582FC',
        padding: padding.xs,
        borderRadius: 3
    },
    deleteButton: {
        backgroundColor: 'red',
        borderRadius: 3,
        marginLeft: 3,
        padding: 5,
    },
});