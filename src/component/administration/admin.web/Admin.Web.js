import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { margin, sizeText } from '../../../styles/values/Dimens';
import ground from '../../../../assets/image/groundAdmin.png';

listData = [
    {
        name: 'DANH MỤC SẢN PHẨM',   
    },
    {
        name: 'SẢN PHẨM',   
    },
    {
        name: 'TIN TỨC',   
    },
    {
        name: 'ẢNH BÌA TRANG CHỦ',   
    },
    {
        name: 'KHÁCH HÀNG',   
    },
    {
        name: 'POPUP',   
    },
    {
        name: 'THONG TIN WEBSITE',   
    },
    {
        name: 'CẤU HÌNH WEBSITE',   
    },
]
export default class AdminWeb extends Component{
    renderList = (data) => {
        return data.map((Element,index) => {
            return(
            <TouchableOpacity style={styles.list} key = {index}>
                <Text style={styles.listText}>{Element.name}</Text>
            </TouchableOpacity>
            );
        });
    }
    render(){
        return(
            <View style={styles.container}>
            {/* phải đặt with, height cho View lớn nhất để ImageBackground hiểu */}
                <HeaderTabs title = {"QUẢN TRỊ WEBSITE"} hidden={false} />
                <ImageBackground source={ground} style={[styles.container,{marginTop: -5}]}>
                    {this.renderList(listData)}
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    list: {
        marginHorizontal: margin.md,
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        marginTop: margin.md,
        paddingBottom: margin.xs
    },
    listText: {
        fontWeight: 'bold',
        fontSize: sizeText.sm
    }
})