import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import { padding, sizeIcon, margin, sizeText } from '../../../styles/values/Dimens';
import Icon from 'react-native-vector-icons/AntDesign';
import { Colors } from '../../../styles/values/Colors';


export default class AddNewCategory extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"THÊM MỚI DANH MỤC"}/>
                <View style={styles.container}>
                    <Text style={styles.text}>Danh mục cha</Text>
                    <View style={styles.input}>
                        <TextInput style={Styles.textInput,styles.textInput} placeholder="Danh mục cha" />
                        <View style={styles.icon}>
                            <Icon name={"caretdown"} size={sizeIcon.lg} color={'white'} />
                        </View>
                    </View>
                    <Text style={styles.text}>Tiêu đề</Text>
                    <TextInput style={[Styles.textInput,{borderRadius:0,color:'#636363', marginBottom:0}]} placeholder="Nhập tiêu đề" />
                    <Text style={styles.text}>Mô tả</Text>
                    <TextInput multiline numberOfLines={8} style={[Styles.textInput,{borderRadius:0,color:'#636363',height:null}]} />
                    <View style={{alignItems: 'flex-start'}}>
                        <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor: Colors.mainColor}]}>
                            <Text style={{color:'white'}}>Thêm mới</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: padding.md
    },
    input: {
        flexDirection: 'row',
        height: 35,
        backgroundColor: '#707070',
        marginTop: margin.xs
    },
    textInput: {
        backgroundColor: '#EBEBEB',
        flex:16,
        paddingLeft: padding.md,
    },
    icon: {
        flex: 2,
        justifyContent:'center',
        alignItems: 'center'
    },
    text: {
        marginTop: margin.xs,
        marginBottom: margin.xs,
        fontSize: sizeText.md,
    }
});