import React,{ Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import styles from './styles';
import Pie from 'react-native-pie';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { sizeIcon } from '../../../styles/values/Dimens';
import Styles from '../../../styles/Styles';

export default class WalletAccount extends Component{
    render(){
        return(
            <View>
                <HeaderTabs title={"VÍ CỦA TÔI"} hidden={false} />
                <View style={styles.infor}>
                    <Text>Thiết lập an toàn bảo mật</Text>
                    <View style={{flexDirection:'row'}}>
                        <Pie marginLeft={10}
                        radius={20}
                        innerRadius={16}
                        sections={[
                          {
                            percentage: 40,
                            color: '#f00',
                          },
                          {
                            percentage: 60,
                            color: '#0f0',
                          },
                        ]}
                        />
                        <Text style={styles.finish}>
                            Hoàn thành các bước sau để tăng độ an toàn bảo mật
                            giúp MOMA hoạt động tốt hơn
                        </Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View>
                            <View style={{flexDirection:'row', marginTop:5}}>
                                <Icon name={"shield-alt"} size={sizeIcon.md} style={{marginHorizontal:5}}></Icon>
                                <Text>Xác thực số điện thoại</Text>
                            </View>
                            <View style={{flexDirection:'row', marginTop:5}}>
                                <Icon name={"shield-alt"} size={sizeIcon.md} style={{marginHorizontal:5}}></Icon>
                                <Text>Xác thực địa chỉ Email</Text>
                            </View>
                            <View style={{flexDirection:'row', marginTop:5}}>
                                <Icon name={"shield-alt"} size={sizeIcon.md} style={{marginHorizontal:5}}></Icon>
                                <Text>Cập nhật thông tin cá nhân</Text>
                            </View>
                        </View>
                        <View style={styles.setting}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('InformationAccount')}
                                style={[Styles.buttonSmall, {backgroundColor:'#037A0B'}]}>
                                <Text style={{color:'white'}}>Thiết lập</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={[styles.item,{paddingTop:14, paddingBottom:14}]}>
                  <Text>Số dư trong ví</Text>
                  <Text style={{fontWeight:'bold', fontSize:15}}>1.200.000 đ</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ManagerAccountBank')} style={styles.item}>
                  <Text>Quản lí thẻ/tài khoản</Text>
                  <Icon style={{transform : [{rotate : '180deg'}]}} name={'backward'} size={sizeIcon.xl} color={'#AAAAAA'}></Icon>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('MoneyTransfer')} style={styles.item}>
                  <Text>Chuyển tiền vào tài khoản</Text>
                  <Icon style={{transform : [{rotate : '180deg'}]}} name={'backward'} size={sizeIcon.xl} color={'#AAAAAA'}></Icon>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('HistoryTransaction')} style={styles.item}>
                  <Text>Lịch sử giao dịch</Text>
                  <Icon style={{transform : [{rotate : '180deg'}]}} name={'backward'} size={sizeIcon.xl} color={'#AAAAAA'}></Icon>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item}>
                  <Text>Quản lí bạn bè</Text>
                  <Icon style={{transform : [{rotate : '180deg'}]}} name={'backward'} size={sizeIcon.xl} color={'#AAAAAA'}></Icon>
                </TouchableOpacity>
            </View>
        );
    }
}