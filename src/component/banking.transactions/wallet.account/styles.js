import { StyleSheet } from 'react-native';
import { padding, margin, border } from '../../../styles/values/Dimens';

const styles = StyleSheet.create({
    infor: {
        padding: padding.sm,
        marginHorizontal: margin.sm,
        backgroundColor: '#DCFEF8',
        marginTop: margin.xs,
        borderColor:'#030F40',
        borderWidth: 1,
        borderRadius: border.sm,
        marginBottom: 10
    },
    setting: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        padding: padding.sm
    },
    finish: {
        marginLeft: margin.xs,
        marginRight: 35, // Text khá ngu, khi trong thẻ View nó có chiều dài bằng chiều dài thẻ,
        //nó không cập nhật lại with nên ta phải marginRight lại đúng bằng các khoảng cách phía bên trái
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: padding.sm,
        borderWidth: 0.3,
        borderColor: '#707070'
    },
    icons: {
        color:'#AAAAAA',
        fontWeight:'bold',
        fontSize: 30
    }
})
export default styles;