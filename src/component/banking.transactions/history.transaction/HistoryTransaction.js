import React, {Component} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { padding, sizeText, margin } from '../../../styles/values/Dimens';

export default class HistoryTransaction extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"LỊCH SỬ GIAO DỊCH"}></HeaderTabs>
                <View style={styles.itemTransaction}>
                    <Text>Mã giao dịch: <Text style={styles.codeText}>MC-025</Text></Text>
                    <Text>Thời gian: 12:05 - 19/12/2019</Text>
                    <Text>Số tiền giao dịch:<Text style={styles.moneyText}> +725 886đ</Text></Text>
                    <Text>Ghi chú giao dịch: Giới thiệu thành công khách hàng đăng ký mới website.</Text>
                    <View style={styles.widthBottom}></View>
                </View>
                <View style={styles.itemTransaction}>
                    <Text>Mã giao dịch: <Text style={styles.codeText}>MC-025</Text></Text>
                    <Text>Thời gian: 12:05 - 19/12/2019</Text>
                    <Text>Số tiền giao dịch:<Text style={styles.moneyText}> +725 886đ</Text></Text>
                    <Text>Ghi chú giao dịch: Giới thiệu thành công khách hàng đăng ký mới website.</Text>
                    <View style={styles.widthBottom}></View>
                </View>
                <View style={styles.itemTransaction}>
                    <Text>Mã giao dịch: <Text style={styles.codeText}>MC-025</Text></Text>
                    <Text>Thời gian: 12:05 - 19/12/2019</Text>
                    <Text>Số tiền giao dịch:<Text style={styles.moneyText}> +725 886đ</Text></Text>
                    <Text>Ghi chú giao dịch: Giới thiệu thành công khách hàng đăng ký mới website.</Text>
                    <View style={styles.widthBottom}></View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    itemTransaction: {
        padding: padding.sm
    },
    codeText: {
        fontWeight: 'bold',
        color: 'blue',
        fontSize: sizeText.sm
    },
    moneyText: {
        fontWeight: 'bold',
        color: 'red',
        fontSize: sizeText.sm
    },
    widthBottom: {
        backgroundColor:'#707070',
        height: 1,
        opacity: 0.5,
        marginTop: margin.sm,
        marginBottom: -10
    }
})