import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Icon from 'react-native-vector-icons/Ionicons';
import { sizeIcon, dimension, padding, margin, border } from '../../../styles/values/Dimens';
import Styles from '../../../styles/Styles';
import vietcombank from '../../../../assets/image/bank/vietcombank.jpg';
import VietinBank from '../../../../assets/image/bank/viettinbank.jpg';
import BIDV from '../../../../assets/image/bank/BIDV.jpg';
import ACB from '../../../../assets/image/bank/ACB.jpg';
import Agribank from '../../../../assets/image/bank/Agribank.png';
import MB from '../../../../assets/image/bank/MB.png';
import Sacombank from '../../../../assets/image/bank/Sacombank.png';
import Shinhan from '../../../../assets/image/bank/Shinhan.jpg';
import TPbank from '../../../../assets/image/bank/TP bank.jpg';
import VIB from '../../../../assets/image/bank/VIB.png';

var dataIconBank1 = [
    {
        image: vietcombank,
        name: "VietcomBank"
    },
    {
        image: VietinBank,
        name: "VietinBank"
    },
    {
        image: BIDV,
        name: "BIDV"
    },
    {
        image: ACB,
        name: "ACB"
    }
]
var dataIconBank2 = [
    {
        image: Agribank,
        name: "Agribank"
    },
    {
        image: MB,
        name: "MB"
    },
    {
        image: VIB,
        name: "VIB"
    },
    {
        image: Shinhan,
        name: "Shinhan"
    },
]
export default class AddCardAccount extends Component{
    rederButtonBank = (data) => {
        return data.map((Elemen, index) => {
            return(
                <TouchableOpacity onPress={() => this.props.navigation.navigate('AddInformationBank')}
                    style={styles.item} key={index}>
                    <Image style={styles.image} source={Elemen.image} />
                    <Text>{Elemen.name}</Text>
                </TouchableOpacity>
            );
        })
    }
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"THÊM TÀI KHOẢN/THẺ"} />
                <View style={styles.input}>
                    <Icon style={{marginLeft:15,marginRight: -10}} name={"md-search"} size={sizeIcon.xl} color={"#ABABAB"} />
                    <TextInput style={[Styles.textInput,styles.textInput]} 
                    placeholder={"Nhập ngân hàng cần tìm"} >
                        {/* <Icon name={"md-search"} size={sizeIcon.lg} color={"#ABABAB"} /> nếu để đây thì nó sẽ không hiện placeholder */}
                    </TextInput>
                </View>
                <View style={styles.container}>
                    {this.rederButtonBank(dataIconBank1)}
                </View>
                <View style={styles.container}>
                    {this.rederButtonBank(dataIconBank2)}
                </View>
                <View style={styles.container}>
                    {this.rederButtonBank(dataIconBank1)}
                </View>
                <View style={styles.container}>
                    {this.rederButtonBank(dataIconBank2)}
                </View>
                <View style={styles.container}>
                    {this.rederButtonBank(dataIconBank1)}
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: padding.md,
        justifyContent: 'space-between',
        marginTop: margin.sm
    },
    item: {
        height: 80,
        width: 90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: 56,
        width: 56,
        borderRadius: 100
    },
    input: {
        flexDirection:'row',
        alignItems:'center',
        borderWidth:0.5,
        marginHorizontal: margin.md,
        height:40,
        borderRadius: border.xs,
        borderColor:'#707070',
        marginTop: margin.xs
    },
    textInput: {
        marginHorizontal: margin.md,
        marginTop: margin.xs,
        borderWidth:0,
        paddingLeft:0,
        height:30
    }
});