import React, {Component} from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { margin, sizeText, dimension } from '../../../styles/values/Dimens';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Styles from '../../../styles/Styles';
import { Colors } from '../../../styles/values/Colors';

export default class ManagerAccountBank extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"QUẢN LÍ TÀI KHOẢN"}></HeaderTabs>
                <View style={styles.container}>
                    <View style={{flexDirection: 'row'}}>
                        <Image style={{width: dimension.fullWidth*0.31, borderRadius: 5}} source={require('../../../../assets/image/A.png')}></Image>
                        <View style={{width: dimension.fullWidth*0.578, marginLeft: dimension.fullWidth*0.028}}>
                            <Text style={styles.linkText}>Liên kết tài khoản ngân hàng</Text>
                            <Text style={{marginTop:5}}>Quản lí chuyển khoản ngân hàng và nâng cao hạn mức giao dịch</Text>
                        </View>
                    </View>
                    <Text style={{marginTop:10}}>
                        {'\t'}{'\t'}{'\t'}Tài khoản liên kết ngân hàng, liên kết thẻ ghi nợ(thẻ Debit) 
                        và cung cấp thông tin ngân hàng nội địa sẽ không hạn chế về số
                        lượng giao dịch và tăng giá trị giao dịch.
                    </Text>
                    <Text>
                        {'\t'}{'\t'}{'\t'}Tài khoản liên kết thẻ tín dụng(thẻ Credit) sẽ không hạn chế về số lượng
                        giao dịch nâng cao giá trị thanh toán khi thanh toán bằng nguồn thẻ tín dụng.
                    </Text>
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddCardAccount')} style={[Styles.buttonSmall, styles.buttonSmall]}>
                            <Text style={{color:'white'}}>Liên kết ngay</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: dimension.fullWidth*0.028,
        marginTop: margin.xs
    },
    linkText: {
        fontWeight: 'bold',
        fontSize: sizeText.sm
    },
    buttonSmall: {
        backgroundColor: Colors.mainColor,
        width: 130,
        marginTop: 10
    }
});