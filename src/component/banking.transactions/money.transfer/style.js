import {StyleSheet} from 'react-native';
import { margin, sizeIcon, padding } from '../../../styles/values/Dimens';

export default styles = StyleSheet.create({
    nameBank: {
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: margin.sm,
        marginTop: margin.xs,
        justifyContent: 'space-between'
    },
    icon: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#919191',
        marginBottom: margin.sm
    },
    money: {
        borderBottomWidth: 3,
        borderTopWidth: 3,
        borderBottomColor: '#C8C8C8',
        borderTopColor: '#C8C8C8',
        padding: padding.sm
    },
    opacityMoney: {
        borderTopWidth: 1,
        opacity: 0.3,
        marginTop: margin.sm
    },
    hotline: {
        color:'red',
        fontSize: 16,
        fontWeight: 'bold'
    }
});
