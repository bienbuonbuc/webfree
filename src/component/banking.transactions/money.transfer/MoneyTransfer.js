import React, {Component} from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import styles from './style';
export default class MoneyTransfer extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"CHUYỂN TIỀN VÀO TÀI KHOẢN"} />
                <TouchableOpacity style={styles.nameBank}>
                    <View>
                        <Text>Chuyển tiền từ Ví MOMA vào</Text>
                        <Text>BIDV - Đầu tư & phát triển Việt Nam</Text>
                    </View>
                    <Text style={styles.icon}> > </Text>
                </TouchableOpacity>
                <View style={styles.money}>
                    <Text>Số tiền muốn rút</Text>
                    <Text>đ 0.00</Text>
                    <Text style={styles.opacityMoney}>Số tiền muốn rút là bội số của 500.000</Text>
                </View>
                <View style={{padding: 10}}>
                    <Text>
                        Lệnh rút tiền được thực hiện chuyển khoản trong cùng ngày
                        làm việc (không tính thứ 7, chủ nhật và ngày lễ)
                    </Text>
                    <Text>
                        Hotline hỗ trợ:
                        <Text style={styles.hotline}> 02477704888</Text>
                    </Text>
                </View>
            </View>
        );
    }
}