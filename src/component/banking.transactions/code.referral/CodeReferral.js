import React, {Component} from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity, TextInput } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import background from '../../../../assets/image/Untitled-1.png';
import { margin, dimension, sizeText, padding } from '../../../styles/values/Dimens';
import Styles from '../../../styles/Styles';
import { Colors } from '../../../styles/values/Colors';

export default class CodeReferral extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"Mã giới thiệu"}></HeaderTabs>
                <Image resizeMode={'contain'} style={styles.imageBackground} source={background}></Image>
                <Text style={styles.shareText}>Chia sẻ mã giới thiệu nhận ưu đãi lớn</Text>
                <Text style={styles.moneyUpgrade}>Nhận chiết khấu lên tới 30% số tiền nâng cấp gói 
                    website của úng tôi khi bạn bè nâng cấp website
                </Text>
                <Text style={styles.codeText}>Mã giới thiệu của bạn</Text>
                <View style={{flexDirection: 'row', paddingHorizontal: dimension.fullWidth*0.1, marginTop: 20}}>
                    <View style={styles.code}>
                        <Text style={{fontWeight:'bold', fontSize: sizeText.sm}}>MOMA 79</Text>
                    </View>
                    <TouchableOpacity style={[Styles.buttonSmall, {backgroundColor: Colors.orange, width: dimension.fullWidth*0.3}]}>
                        <Text>Copy mã</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={[Styles.buttonSmall,{backgroundColor:Colors.mainColor, marginHorizontal:dimension.fullWidth*0.1, marginTop:5, height:35}]}>
                    <Text style={{color:'white'}}>Chia sẻ mã giới thiệu</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    imageBackground: {
        height: dimension.fullHeight*0.3,
        marginTop: margin.xs
    },
    shareText: {
        textAlign:'center',
        fontWeight: 'bold',
        fontSize: sizeText.sm
    },
    moneyUpgrade: {
        marginHorizontal: 20,
        textAlign:'center',
        marginTop: margin.xs
    },
    codeText: {
        fontWeight: 'bold',
        fontSize: sizeText.sm,
        textAlign: 'center',
        marginTop: margin.md
    },
    code: {
        borderWidth: 2,
        borderColor: Colors.backgroundTextInput,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        width: dimension.fullWidth*0.48,
        marginRight: dimension.fullWidth*0.02,
        height: 35
    }
})