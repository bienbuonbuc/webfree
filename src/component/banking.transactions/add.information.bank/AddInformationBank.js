import React, {Component} from 'react';
import { View, TextInput, StyleSheet, Text, TouchableOpacity } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { margin, sizeText } from '../../../styles/values/Dimens';
import { Colors } from '../../../styles/values/Colors';
import Styles from '../../../styles/Styles';

export default class AddInformationBank extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"VIETCOMBANK"}></HeaderTabs>
                <TextInput style={styles.textInput} placeholder="Số thẻ"></TextInput>
                <TextInput style={styles.textInput} placeholder="Ngày phát hành thẻ"></TextInput>
                <TextInput style={styles.textInput} placeholder="Họ và tên chủ thẻ"></TextInput>
                <TextInput style={styles.textInput} placeholder="CMND/CCCD/Hộ chiếu"></TextInput>
                <Text style={styles.note}>Lưu ý: số điện thoại đăng ký cần trùng với số điện thoại trong thông tin cá nhân</Text>
                <View style={{alignItems: 'flex-start'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('WalletAccount')}
                        style={[Styles.buttonSmall, styles.buttonSmall]}>
                        <Text style={{color:'white'}}>Tiếp tục</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    textInput: {
        marginHorizontal: margin.md,
        marginTop: margin.md,
        borderBottomWidth: 1,
        borderBottomColor: Colors.backgroundTextInput,
        fontSize: sizeText.sm
    },
    note: {
        color: 'red',
        marginLeft: margin.md,
        marginTop: margin.sm
    },
    buttonSmall: {
        backgroundColor:Colors.blueButton,
        width: 100,
        marginLeft: margin.md,
        marginTop: margin.sm
    }
})