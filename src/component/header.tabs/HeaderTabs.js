import React, {Component} from 'react';
import { View, Text, TouchableOpacity, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
// import Icon from 'react-native-vector-icons/FontAwesome5';   xem thư viện nào có ảnh phù hợp thì import vào để dùng Icon
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Styles from '../../styles/Styles';
import {iconName} from '../../styles/values/String';
import *as Dimens from '../../styles/values/Dimens';

export default class HeaderTabs extends Component{
    render(){
        return(
            <View style={Styles.headerTab}>
        {/* <View style={[styles.headerTab, {height: this.props.height, width: this.props.width}]} >
            Cái nào style tĩnh thì tách riêng, còn chứa tham số thì làm trực tiếp
            style nhận vào tham số là 1 object hoặc là 1 mảng các object */}
                <StatusBar hidden={true}></StatusBar>
                <View style={Styles.titleTab}>    
                        <Text style={Styles.titleHeader}>{this.props.title}</Text>
                </View>
                { this.props.hidden === false ?undefined:
                <TouchableOpacity onPress={() => this.props.back()} style={Styles.backIconHeader}>
                        <Icon name={iconName.back} size={Dimens.sizeIcon.xl} color={'white'}></Icon>
                </TouchableOpacity> 
                }
            </View>
        );
    }
}