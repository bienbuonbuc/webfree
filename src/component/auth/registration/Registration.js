import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Image,
    Platform,
    Dimensions,
    Text,
    TextInput,
    TouchableOpacity,
    Alert,
    AsyncStorage
  } from 'react-native';
import bkg from '../../../../assets/image/background.png';
import logo from '../../../../assets/image/dangnhap.png';
import styles from './styles';
import Styles from '../../../styles/Styles';
import Http from '../../../fetch.api/Networking';

export default class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            domain: '',
            email: '',
            phone: '',
            name_home: '',
            password: '',
            password_confirmation: '',
            theme_code: 'xhome',
            hidden: false,

        })
    }

    async postRegistration () {
        try{
        let data = {
            domain: this.state.domain,
            email: this.state.email,
            phone: this.state.phone,
            name: this.state.name_home,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation,
            theme_code: this.state.theme_code
        }
        let response = await Http.post('create-theme', data);
        if(response.status == 200) {
            console.log('vao')
        }
       
        //console.log(response.message);
        const array = Object.values(response.message);
        array.map(function (elem, i) {
           console.log(elem)
        })
        // if(response.message) {
        //     console.log(response.message)
        //     console.log(response.message.email == null)
        //     if(response.message.email== null)
        //         {
        //         console.log('aaaaaaaaaaaaaaaa')}

        //     // alert(response.message.domain[0] +  response.message.email[0] + response.message.password[0] + response.message.phone[0])
        // }
        }catch(e) {
            console.log(e)
        }
    }
    
    render() {
        return (
                <ImageBackground source={bkg} style={styles.backGround}>
                    <View style={styles.headerLogo}>
                        <Image source={logo} style={styles.logo}></Image>
                    </View>
                    {/* <Svg height="40" width="500">        
                    <Text x={Platform.OS === 'ios'? 25 : 60} y="25" fill="#0391FF" fontSize="18" fontWeight='bold' >
                        Hệ thống Quản trị Website miễn phí
                    </Text>
                    </Svg> */}
                    <Text style={styles.titleLogin} >
                        Hệ thống Quản trị Website miễn phí
                    </Text>
                    <View style = {styles.loginForm}>
                        <Text style={styles.titleForm}>ĐĂNG KÍ</Text>
                        <TextInput style={[Styles.textInput, {height:35}]} 
                            placeholder={"Nhập địa chỉ website"}
                            onChangeText = {(text) => this.setState({ domain: text })}
                        />
                        <TextInput style={[Styles.textInput, {height:35}]} 
                            placeholder={"Nhập tên"}
                            onChangeText = {(text) => this.setState({ name_home: text })}
                        />
                        <TextInput style={[Styles.textInput, {height:35}]} 
                            placeholder={"Nhập địa chỉ Email"}
                            onChangeText = {(text) => this.setState({ email: text })}
                        />
                        <TextInput style={[Styles.textInput, {height:35}]} 
                            placeholder={"Nhập số điện thoại"}
                            onChangeText = {(text) => this.setState({ phone: text })}
                        />
                        <TextInput style={[Styles.textInput]}
                            placeholder={"Nhập mật khẩu"}
                            secureTextEntry
                            keyboardType="default"
                            underlineColorAndroid='transparent'
                            onChangeText = {(text) => this.setState({ password: text })}
                        />
                        {this.state.hidden == true?
                            <Text>Mật khẩu không trùng khớp</Text>
                            :undefined
                        }
                        <TextInput style={[Styles.textInput]}
                            placeholder={"Nhập lại mật khẩu"}
                            secureTextEntry
                            keyboardType="default"
                            underlineColorAndroid='transparent'
                            onChangeText = {async(text) => {
                                await this.setState({ password_confirmation: text })
                               await console.log(this.state.password != this.state.password_confirmation)
                                 if( (this.state.password_confirmation != this.state.password) )
                                   await console.log('yyyyyyyy')
                                // if(this.state.password_confirmation == this.state.password)
                                //     this.setState({hidden:false})
                                }}
                        />
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <View style={{flexDirection:'row', marginTop: 20}}>
                                    <Text style={{fontWeight: 'bold', color: "#0391FF"}}>{`<<`}</Text>
                                    <Text style={{color: "#0391FF",textAlign: 'left' }}>
                                        {`  Quay lại đăng nhập`}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                                this.postRegistration();
                                // this.setState({alert: true})
                            }}
                            style={styles.loginBtn}>
                            <Text style={styles.text}>
                                ĐĂNG KÍ
                            </Text>
                        </TouchableOpacity>
                    </View>
                    {/* <Svg height={300} width={400} >
                    <Text x={Platform.OS==='ios' ? 48 : dimension.fullWidth*0.15} y={Platform.OS==='ios' ? 160 : dimension.fullHeight*0.24} fontSize={18} stroke="black" strokeWidth="0.1" fill="#e9993e" fontWeight='bold'>
                        Hotline hỗ trợ: 024 777 04 888
                    </Text>
                    </Svg> */}
                    <View style={{flex: 1, justifyContent: 'flex-end'}}>
                    <Text style={styles.titleHelp} >
                            Hotline hỗ trợ: 024 777 04 888
                    </Text>
                </View>
            </ImageBackground>
        );
    }
}