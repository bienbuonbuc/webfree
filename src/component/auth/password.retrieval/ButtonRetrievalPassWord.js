import React from 'react';
import styles from './styles';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import Styles from '../../../styles/Styles';
// import {patch} from '../../../patchs';

export default class ButtonRetrievalPassWord extends React.Component{
    render(){
        return(
            <View style = {styles.loginForm}>
                <Text style={styles.titleForm}>LẤY LẠI MẬT KHẨU</Text>
                {/* <patch.Textinput height={35} width={null} placeholder={'Nhập địa chỉ website'} ></patch.Textinput>
                <patch.Textinput height={35} width={null} placeholder={'Nhập Email'} ></patch.Textinput> */}
                <TextInput style={[Styles.textInput]} 
                    placeholder={"Nhập địa chỉ website"}
                />
                <TextInput style={Styles.textInput} 
                    placeholder={"Nhập địa chỉ Email"}
                />
                <TouchableOpacity style={styles.loginBtn}>
                    <Text style={styles.text}>
                        LẤY LẠI MẬT KHẨU
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.backScreen()}>
                    <View style={{flexDirection:'row', marginTop: 20}}>
                        <Text style={{fontWeight: 'bold', color: "#0391FF"}}>{`<<`}</Text>
                        <Text style={{color: "#0391FF",textAlign: 'left' }}>
                            {`  Quay lại đăng nhập`}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}