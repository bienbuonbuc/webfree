import React from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  Image,
  TextInput,
  Platform,
  TouchableOpacity,
  Text
} from 'react-native';
import bkg from '../../../../assets/image/background.png';
import logo from '../../../../assets/image/dangnhap.png';
// import Svg, { Text } from "react-native-svg";
import styles from './styles';
import ButtonRetrievalPassWord from './ButtonRetrievalPassWord';
import {dimension, sizeText, margin} from '../../../styles/values/Dimens';
import { Colors } from '../../../styles/values/Colors';
export default class PasswordRetrieval extends React.Component {
  render() {
    return (
      <ImageBackground source={bkg} style={styles.backGround}>
          <View style={styles.headerLogo}>
              <Image source={logo} style={styles.logo}></Image>
          </View>
          {/* <Svg height="40" width="500">        
              <Text x={Platform.OS === 'ios'? 25 : 60} y="25" fill="#0391FF" fontSize="18" fontWeight='bold' >
                  Hệ thống Quản trị Website miễn phí
              </Text>
          </Svg> */}
              <Text style={styles.titleLogin} >
                  Hệ thống Quản trị Website miễn phí
              </Text>
          <ButtonRetrievalPassWord backScreen= {() => this.props.navigation.goBack()} />
          {/* <Svg height={300} width={400} >
              <Text x={Platform.OS==='ios' ? 48 : dimension.fullWidth*0.15} y={Platform.OS==='ios' ? 180 : dimension.fullHeight*0.24} fontSize={18} stroke="black" strokeWidth="0.1" fill="#e9993e" fontWeight='bold'>
                  Hotline hỗ trợ: 024 777 04 888
              </Text>
          </Svg> */}
          <View style={{flex: 1, justifyContent: 'flex-end'}}>
              <Text style={styles.titleHelp} >
                      Hotline hỗ trợ: 024 777 04 888
              </Text>
          </View>
      </ImageBackground>
    );
  }
}