import React, { Component } from 'react';
import styles from './style';
import Styles from '../../../styles/Styles';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Alert,
    AsyncStorage
  } from 'react-native';

export default class ButtonLogIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            domain: 'http://vietcomputer.moma.vn',
            email: 'buituanviet1234@gmail.com',
            password: 'matkhau1',
            // domain: '',
            // email: '',
            // password: '',
            alert: false
        }
    }
    async componentWillReceiveProps(prevPropps) {
        var a= await this.props.data.error;
        var b= await this.props.data.token;
        //hai biến này có tác dụng dừng hàm, để cập nhật giá trị data
        //rồi mới chuyển xuống các lệnh if, nếu ko có sẽ chưa kịp lưu giá trị mới đã xuống if
        if(this.props.data.token){
            try {
                await AsyncStorage.setItem('token', this.props.data.token)
                await AsyncStorage.setItem('domain', this.state.domain)
            }catch(e){
                console.log('lỗi cho vào AsynStorege')
            }
            this.props.nextMain();
            return;
        }
        if(this.props.data.error)
        {
            Alert.alert(this.props.data.error)
            
        }
    }
    render() {
        return(
            <View style = {styles.loginForm}>
                <Text style={styles.titleForm}>ĐĂNG NHẬP</Text>
                <TextInput style={[Styles.textInput, {height:35}]} 
                    placeholder={"Nhập địa chỉ website"}
                    onChangeText = {(text) => this.setState({ domain: text })}
                />
                <TextInput style={[Styles.textInput, {height:35}]} 
                    placeholder={"Nhập địa chỉ Email"}
                    onChangeText = {(text) => this.setState({ email: text })}
                />
                <TextInput style={[Styles.textInput]}
                    placeholder={"Nhập mật khẩu"}
                    secureTextEntry
                    keyboardType="default"
                    underlineColorAndroid='transparent'
                    onChangeText = {(text) => this.setState({ password: text })}
                />
                    <Text style={{ color: "#0391FF", textAlign: 'left' }} onPress={()=> this.props.nextPasswordRetrieval()}>
                        Bạn quên mật khẩu ?
                    </Text>
                <TouchableOpacity onPress={() => {
                        const {email,domain,password} = this.state;
                        this.props.onClickLogIn({email:email,domain:domain,password:password})
                        // this.setState({alert: true})
                    }}
                    style={styles.loginBtn}>
                    <Text style={styles.text}>
                        ĐĂNG NHẬP
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}