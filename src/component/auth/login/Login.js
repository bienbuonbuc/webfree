import React from 'react';
import {
  View,
  ImageBackground,
  Image,
  Platform,
  Dimensions,
  Text,
} from 'react-native';
// import Svg, { Text } from "react-native-svg";
import bkg from '../../../../assets/image/background.png';
import logo from '../../../../assets/image/dangnhap.png';
import ButtonLogIn from './ButtonLogIn';
import styles from './style';
import LogInContainers from '../../../containers/auth.container/login.containers/LogInContainers';
import { dimension } from '../../../styles/values/Dimens';
import { TouchableOpacity } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

export default class Login extends React.Component {
  render() {
    return (
      <ImageBackground source={bkg} style={styles.backGround}>
            <View style={styles.headerLogo}>
                <Image source={logo} style={styles.logo}></Image>
            </View>
            {/* <Svg height="40" width="500">        
              <Text x={Platform.OS === 'ios'? 25 : 60} y="25" fill="#0391FF" fontSize="18" fontWeight='bold' >
                  Hệ thống Quản trị Website miễn phí
              </Text>
            </Svg> */}
            <Text style={styles.titleLogin} >
                  Hệ thống Quản trị Website miễn phí
            </Text>
            <LogInContainers nextPasswordRetrieval={()=>this.props.navigation.navigate('PasswordRetrieval')} 
                nextMain = {() =>
                    this.props.navigation.navigate('mainBottom')
                }
            />
            {/* <Svg height={300} width={400} >
              <Text x={Platform.OS==='ios' ? 48 : dimension.fullWidth*0.15} y={Platform.OS==='ios' ? 160 : dimension.fullHeight*0.24} fontSize={18} stroke="black" strokeWidth="0.1" fill="#e9993e" fontWeight='bold'>
                  Hotline hỗ trợ: 024 777 04 888
              </Text>
            </Svg> */}
            <View style={{flex: 1, justifyContent: 'flex-end'}}>
              <Text style={styles.titleHelp} >
                      Hotline hỗ trợ: 024 777 04 888
              </Text>
          </View>
      </ImageBackground>

    );
  }
}