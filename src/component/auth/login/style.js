import {StyleSheet, Dimensions} from 'react-native';
import *as Dimens from '../../../styles/values/Dimens';
const { width: WIDTH } = Dimensions.get('window');
import {sizeText, margin} from '../../../styles/values/Dimens';
import { Colors } from '../../../styles/values/Colors';

const styles = StyleSheet.create({
    backGround: {
      flex: 1, 
      flexDirection : 'column'
    },
    logo: {
      width: "100%",
      height: Dimens.dimension.fullHeight*0.15,
      resizeMode : 'contain'
    },
    headerLogo: {
      paddingTop : 30,
      alignItems: 'center',
      justifyContent: 'center'
    },
    loginForm : {
      marginTop : 30,
      borderColor : '#a6a6a6',
      borderRadius : Dimens.border.sm,
      marginHorizontal: 30,
      padding : Dimens.padding.md,
      backgroundColor: 'rgba(255,255,255,1)',
     
    },
    loginBtn: {
      marginTop: Dimens.margin.sm,
      height: 30,
      borderRadius: Dimens.border.sm,
      justifyContent: "center",
      backgroundColor: '#0391FF',
      marginHorizontal: WIDTH*0.16
    },
    text: {
      textAlign: "center",
      color: "white",
      fontWeight: "bold",
    },
    titleForm: {
      textAlign:'center',
      fontWeight: 'bold',
      fontSize: Dimens.sizeText.sm,
      color: "#0391FF",
      marginBottom : Dimens.margin.lg,
      marginTop: Dimens.margin.xs
    },
      // please: {
      //   textAlign:'center',
      //   fontWeight:"bold",
      //   fontSize: 10,
      //   color: "#e9993e",
      // textShadowOffset: { width:1, height:1 }, //kích thước bóng đổ
      // textShadowRadius: 0.2, //bán kín bóng
      // textShadowColor: '#000',
      // }
      titleLogin: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: sizeText.md,
        color: Colors.orange,
        marginTop: 10
      },
      titleHelp: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: sizeText.md,
        color: Colors.orange,
        marginBottom:margin.sm
      }
  });
export default styles;  