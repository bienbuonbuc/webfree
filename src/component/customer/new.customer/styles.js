import {StyleSheet, Dimensions} from 'react-native';
const { width: WIDTH } = Dimensions.get('window');
const { height: HEIGHT } = Dimensions.get('window');
import *as Dimens from '../../../styles/values/Dimens';

const styles = StyleSheet.create({
    container: {
      flex : 1,
      backgroundColor: '#fff',
    },
    header: {
      backgroundColor: '#0090FF',
      width: WIDTH,
      height: 50,
      flexDirection:'row',
      alignItems:'center',
      paddingHorizontal: Dimens.padding.sm,
      justifyContent:'space-between',
      paddingTop: Dimens.padding.lg
    },
    input: {
        marginRight: 5,
        width: WIDTH*0.76
    },
  
    content : {
      paddingLeft : Dimens.padding.sm,
      paddingRight : Dimens.padding.sm,
      backgroundColor:'white'
    },
    customerInfo : {
      borderRadius : Dimens.border.xs,
      backgroundColor : "white",
      marginTop: Dimens.margin.xs,
      height: 50,
      borderBottomWidth: 0.5,
      borderBottomColor: '#707070',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding:1
      
    },
    viewpicker: {
        flexDirection:'row',
        height: 40,
    },
    picker: {
        width: 120,
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
    }
  });
export default styles;  