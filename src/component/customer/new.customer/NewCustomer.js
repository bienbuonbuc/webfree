import React from 'react';
import {Text, View, Platform, Dimensions, TouchableOpacity,StatusBar,TextInput,ScrollView, FlatList, AsyncStorage } from 'react-native';
import styles from './styles';
import { Container, Header, Content, Picker, Form } from "native-base"; 
import Icon from 'react-native-vector-icons/Ionicons';
import { dimension } from '../../../styles/values/Dimens';

const timeCustomer = [
  {
  label: "Hôm nay",
  value: "key0"
},
{
  label: "Tuần này",
  value: "key1"
},
{
  label: "Tuần trước",
  value: "key2"
},
{
  label: "tháng này",
  value: "key3"
},
{
  label: "Tháng trước",
  value: "key4"
},
{
  label: "năm nay",
  value: "key5"
},
{
  label: "năm trước",
  value: "key6"
},
]
const customer = [
  {
    label:"KH mới",
    value: "key0"
},
{
  label:"KH tiếp cận",
  value: "key1"
},
{
  label:"KH mua hàng",
  value: "key2"
},
{
  label:"KH hoàn đơn",
  value: "key3"
},
]
export default class NewCustomer extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        selected: "key1"
      };
    }
    onValueChange = (value) => {
      this.setState({
        selected: value
      });
    }
    renderItemPicker = (itemDataSource) => {
        return itemDataSource.map( (Element, index) => {
            return(
              <Picker.Item key = {index} label = {Element.label} value = {Element.value}></Picker.Item>
            );
        }
        );
    }
    // renderFlatList = (a) =>{
    //     return a.map((Element, index) => {
    //       return(
    //           <TouchableOpacity onPress={() => this.props.navigation.navigate('InformationCustomer')} key={index}>
    //               <View style={styles.customerInfo}>
    //                   <View>         
    //                       <Text>Nguyễn Văn Biên - 0586895874</Text>
    //                       <Text>bienbuonbuc@gmail.com</Text>
    //                   </View> 
    //                   <Text>29/11</Text>
    //               </View>
    //           </TouchableOpacity>
    //         )
    //     })
    // }
    async componentDidMount() {
      const token = await AsyncStorage.getItem('token');
      const domain = await AsyncStorage.getItem('domain');
      this.props.renderNewCustomer(token, domain)
    }
    render() {
      // console.log('tessssttttt',this.props.data)
        return (
            <View style={styles.container}> 
                <StatusBar hidden={true}></StatusBar>
                <View style={styles.header}>
                    <Icon name="md-search" size={25} color="white" ></Icon>
                    <TextInput style={styles.input} placeholder = "Tìm tên, Tags khách hàng"></TextInput>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddCustomers')}>
                        <Icon name="md-add" size={25} color="white" ></Icon>
                    </TouchableOpacity>
                </View>
  
                <View style={styles.viewpicker}>
                  <View style={{justifyContent:'center', marginLeft: 10}}>
                    <Text>Thống kê</Text>
                  </View>
                  <View style={styles.picker}>
                    <Container style={{backgroundColor:'#E1DEDE', borderRadius: 5}}>
                        <Content>
                          <Form>
                            <Picker
                              note
                              mode="dropdown"
                              style={{ width:120, height:35}}
                              selectedValue={this.state.selected}
                              onValueChange={this.onValueChange.bind(this)}
                            >
                              {this.renderItemPicker(timeCustomer)}
                            </Picker>
                          </Form>
                        </Content>
                    </Container>
                    </View>

                    <View style={styles.picker}>
                    <Container style={{backgroundColor:'#E1DEDE', borderRadius: 5, marginRight: -10}}>
                        <Content>
                          <Form>
                            <Picker
                              note
                              mode="dropdown"
                              style={{ width:Platform.OS==='ios' ? 150 : 130, height:35}}
                              selectedValue={this.state.selected}
                              onValueChange={this.onValueChange.bind(this)}
                              
                            >
                              {this.renderItemPicker(customer)}
                            </Picker>
                          </Form>
                        </Content>
                    </Container>
                    </View>
                </View>
                
            
                <View style={styles.content}>
                    {/* <ScrollView showsVerticalScrollIndicator={false}>
                        {this.renderFlatList(timeCustomer)}
                    </ScrollView> */}
                    <FlatList
                        data = {this.props.data}
                        renderItem = {({item,index}) => {
                            return(
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('InformationCustomer',{contact_id:this.props.data[index].contact_id})}>
                                    <View style={styles.customerInfo}>
                                        <View style={{width: '75%'}}>         
                                            <Text>{this.props.data[index].name} - {this.props.data[index].phone}</Text>
                                            <Text>{this.props.data[index].email}</Text>
                                        </View> 
                                        <Text>{this.props.data[index].created_at!=null?this.props.data[index].created_at.substr(0,10):'Chưa xác định'}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }}
                        keyExtractor = {(item,index) => index.toString()}
                    >
                    </FlatList>
                </View>
            </View>
        );
    };
}