import React, { Component } from 'react';
import { StyleSheet, Text, View, Image,Platform , Dimensions, TouchableOpacity, Picker, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import user from '../../../../assets/image/admin.jpg';
import bkg3 from '../../../../assets/image/bkg.jpg';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import *as Dimens from '../../../styles/values/Dimens';
import { Colors } from '../../../styles/values/Colors';
// import {patch} from '../../../patchs';

const namePlace = [
    {
    name: "Họ và tên",
},
{
    name: "Số điện thoại",
},
{
    name: "Email",
},
{
    name: "Tag",
},
{
    name: "Địa chỉ",
}
]
export default class  AddCustomers extends Component {
    state = {user: ''}
    updateUser = (user) => {
        this.setState({ user: user })
    }
    // renderTextInput = (dataPlaceholder) => {
    //     return dataPlaceholder.map( (Element, index) =>{
    //             return(
    //                 <View key={index}>
    //                     <Text>{Element.name}</Text>
    //                     <patch.Textinput placeholder={Element.name}></patch.Textinput>
    //                 </View>
    //         );}
    //     )
    // }
    renderTextInput = (dataPlaceholder) => {
        return dataPlaceholder.map( (Element, index) =>{
                return(
                    <View key={index}>
                        <Text style={{marginBottom:5}}>{Element.name}</Text>
                        <TextInput style={Styles.textInput} placeholder={Element.name}></TextInput>
                    </View>
            );}
        )
    }
    render() {
        return (
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"THÊM MỚI KHÁCH HÀNG"}/>
                <ScrollView>
                    <View style={styles.content2}>
                        {this.renderTextInput(namePlace)}
                        <Text>Ghi chú</Text> 
                        <TextInput style={styles.textarea} 
                            underlineColorAndroid="transparent"
                            placeholderTextColor="grey"
                            numberOfLines={4}
                            multiline={true}
                            />                       
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('NewCustomer')} 
                            style={[styles.button, Styles.buttonSmall]}>
                            <Text style={{color:"white", fontSize: 14}}>Thêm mới</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    content2: {
        paddingLeft : Dimens.padding.md,
        paddingRight : Dimens.padding.md,
        marginTop: Dimens.margin.sm
    },
    inputForm: {
        height : 45,
        borderRadius : 3,
        borderWidth : 1,
        borderColor : "#a6a6a6",
        paddingLeft : Dimens.padding.sm,
        paddingRight : Dimens.padding.sm,
        marginBottom : Dimens.margin.xs,
        marginTop : Dimens.margin.xs,
        color : "#969696",
        backgroundColor : "white",
        fontSize : 13 
    },
    textarea: {
        backgroundColor : "white",
        textAlignVertical: "top",
        marginBottom: Dimens.margin.sm,
        marginTop: Dimens.margin.sm,
        padding :Dimens.padding.sm,
        borderRadius : 3,
        borderWidth : 1,
        borderColor : "#a6a6a6",
    },
    button: {
        backgroundColor:Colors.mainColor,
        alignSelf : "flex-start"
    }
})
