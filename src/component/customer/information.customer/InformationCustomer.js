import React, {Component} from 'react';
import {View, Text, AsyncStorage} from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {margin, sizeIcon} from '../../../styles/values/Dimens';
import Styles from '../../../styles/Styles';
import OrderCustomer from './order.customer/OrderCustomer';
import RemindCustomer from './remind.customer/RemindCustomer';
import StatusCustomer from './status.customer/StatusCustomer';
import { Colors } from '../../../styles/values/Colors';
import {ScrollableTabView} from '@valdio/react-native-scrollable-tabview';
import { connect } from 'react-redux';
import { informationCustomerAction } from '../../../actions/actions';

const array = [
    {
        name: "Họ và tên: ",
        information: "Nguyễn Văn Biên"
    },
    {
        name: "Số điện thoại: ",
        information: "0978206442"
    },
    {
        name: "Email: ",
        information: "bienbuonbuc@gmail.com"
    },
    {
        name: "Địa chỉ: ",
        information: "Ngõ 773 giải phóng, Giáp bát, Hà Nội"
    },
    {
        name: "Tags: ",
        information: "Khách hàng chưa mua hàng"
    },
    {
        name: "Ghi chú: ",
        information: "Khách hàng pro"
    },
    {
        name: "Nhóm khách hàng: ",
        information: "Khách hàng mới"
    }
];
class InformationCustomer extends React.Component{
    renderInformation = (data) => {
        return data.map((Element, index)=>{
            return(
                <View key={index} style={{flexDirection:'row', marginTop: 10}}>
                    <Text style={styles.textBold}>
                        {Element.name}
                    </Text>
                    {/* {index==0
                        ?<Text>{this.props.data.name}</Text>
                    :index==1?<Text>{this.props.data.phone}</Text>
                    :index==2?<Text>{this.props.data.email}</Text>
                    :index==3?<Text>{this.props.data.address}</Text>        cách 1
                    :index==4?<Text>{this.props.data.tag}</Text>
                    :index==5?<Text>{Element.information}</Text>
                        :<Text>{Element.information}</Text>

                    } */}
                    <Text>{Element.information}</Text>
                    {/* cách 2: cho biến array vào render để lấy các giá trị trong Element, array trên cùng là bỏ
                        vì array đó không lấy được giá trị this.props.data */}
                </View>
            );
        })
    }
    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        const domain = await AsyncStorage.getItem('domain');
        this.props.onClickInformationCustomer(token, domain, this.props.navigation.getParam('contact_id'));
    }

    render(){
        const array = [
            {
                name: "Họ và tên: ",
                information: this.props.data.name
            },
            {
                name: "Số điện thoại: ",
                information: this.props.data.phone
            },
            {
                name: "Email: ",
                information: this.props.data.email
            },
            {
                name: "Địa chỉ: ",
                information: this.props.data.address
            },
            {
                name: "Tags: ",
                information: this.props.data.tag
            },
            {
                name: "Ghi chú: ",
                information: "Khách hàng pro"
            },
            {
                name: "Nhóm khách hàng: ",
                information: "Khách hàng mới"
            }
        ];
        return(
            <View style={{flex:1}}>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"THÔNG TIN KHÁCH HÀNG"}></HeaderTabs>
                <View style={styles.viewInformation}>
                    <View style={styles.viewButton}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('EditCustomer',{contact_id:this.props.navigation.getParam('contact_id')})} 
                            style={[styles.editButton, Styles.buttonSmall]}>
                            <Icon name={"edit"} color={"white"} size={sizeIcon.md} />
                            <Text style={styles.textButton}>Chỉnh sửa</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.deleteButton, Styles.buttonSmall]} >
                            <Icon name={"delete-forever"} color={"white"} size={sizeIcon.md} />
                            <Text style={styles.textButton}>Xóa</Text>
                        </TouchableOpacity>
                    </View>
                    {this.renderInformation(array)}
                </View>
                <ScrollableTabView tabBarActiveTextColor ={"black"} locked = {true} showsHorizontalScrollIndicator={false}
                                   tabBarInactiveTextColor={Colors.mainColor} tabBarUnderlineStyle={{backgroundColor:'black', height: 1}}
                                   tabBarTextStyle = {{}}
                                   style={{borderTopWidth: 7, borderTopColor: 'lightgray', marginTop: margin.sm, alignItems: 'center', }}
                >
                    <StatusCustomer tabLabel="Trạng thái" />
                    <RemindCustomer tabLabel="Nhắc nhở" />
                    <OrderCustomer tabLabel="Đơn hàng" />
                </ScrollableTabView>
            </View>
            
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data : state.InformationCustomerReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickInformationCustomer: (token, domain, contact_id) => {
            dispatch(informationCustomerAction(token, domain, contact_id));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(InformationCustomer);