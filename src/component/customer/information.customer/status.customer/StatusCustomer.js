import React, {Component} from 'react';
import {Text, View, TextInput, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import Styles from "../../../../styles/Styles";
import * as Dimens from "../../../../styles/values/Dimens";
import {padding} from "../../../../styles/values/Dimens";
import Icon from 'react-native-vector-icons/Ionicons'
import {sizeIcon} from "../../../../styles/values/Dimens";
import {margin} from "../../../../styles/values/Dimens";
import { Colors } from '../../../../styles/values/Colors';

export default class StatusCustomer extends React.Component {
    render(){
        return(
          <View style={styles.container}>
              <TextInput    style={[styles.textarea]}
                            placeholder={"Ghi chú"}
                            placeholderTextColor="grey"
                            numberOfLines={3}
                            multiline={true}
              />
              <TouchableOpacity style={[Styles.buttonSmall, styles.buttonSmall]}>
                  <Text style={{color: 'white'}}>Ghi</Text>
              </TouchableOpacity>
              <Text style={styles.textInformation}>- 12/11-khách hàng đã đọc email từ chiến dịch</Text>
              <Text style={styles.textInformation}>- 09/11-Khách hàng đã thanh toán 2.500.000 nvd</Text>
              <Text style={styles.textInformation}>- 05/11-Đã tiếp cận được khách hàng</Text>
              <View style={[styles.textInformation,{flexDirection: 'row', alignItems: 'center'}]}>
                  <Text style={{marginRight: 5}}>
                      - 02/11-Đã gọi với khác hàng
                  </Text>
                  <Icon name={"ios-play-circle"} color={"#168501"} size={sizeIcon.md}></Icon>
                  <Text style={{marginLeft: 5}}>3 phút 20 giây</Text>
              </View>
          </View>
        );
    }
}
const styles = StyleSheet.create({
    textarea: {
        backgroundColor: "white",
        textAlignVertical: "top",
        marginBottom: Dimens.margin.sm,
        padding: Dimens.padding.sm,
        borderRadius: 3,
        borderWidth: 1,
        borderColor: "#a6a6a6",
    },
    container: {
        padding: padding.md
    },
    buttonSmall: {
        backgroundColor: Colors.blueButton,
        width: 80,
        marginBottom: margin.xs
    },
    textInformation: {
        borderBottomWidth:Platform.OS==='ios' ? 0 : 0.4,
        borderBottomColor: 'rgba(0,0,0,0.2)',
        marginTop: margin.xs,
    }
})