import {StyleSheet} from 'react-native';
import { Colors } from '../../../styles/values/Colors';
import { padding, margin, sizeText, border } from '../../../styles/values/Dimens';

const styles = StyleSheet.create({
    viewButton: {
        flexDirection: 'row',
    },
    editButton: {
        backgroundColor: Colors.mainColor,
        flexDirection: 'row',
    },
    deleteButton: {
        backgroundColor: 'red',
        flexDirection: 'row',
        marginLeft: margin.md,
    },
    textButton: {
        color: 'white',
        fontSize: sizeText.xs,
        marginLeft: margin.xs
    },
    viewInformation: {
        paddingHorizontal: padding.md,
        marginTop: margin.xs,
    },
    textBold: {
        fontWeight: 'bold',
        color: Colors.mainColor,
    }
})
export default styles;