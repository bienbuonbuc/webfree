import React, { Component } from 'react';
import { StyleSheet, Text, View, Image,ImageBackground , Dimensions, TouchableOpacity, Picker, TextInput, ScrollView, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import user from '../../../../assets/image/admin.jpg';
import bkg3 from '../../../../assets/image/bkg.jpg';
import HeaderTabs from '../../header.tabs/HeaderTabs';
import Styles from '../../../styles/Styles';
import *as Dimens from '../../../styles/values/Dimens';
import { connect } from 'react-redux';
import { editCustomerAction } from '../../../actions/actions';
// import {patch} from '../../../patchs';

const namePlace = [
    {
        name: "Họ và tên",
    },
    {
        name: "Số điện thoại",
    },
    {
        name: "Email",
    },
    {
        name: "Địa chỉ",
    }
]
class EditCustomer extends Component {
    // state = {user: ''}
    // updateUser = (user) => {
    //     this.setState({ user: user })
    // }
    
    // renderTextInput = (dataPlaceholder) => {
    //     return dataPlaceholder.map( (Element, index) =>{
    //             return(
    //                 <View key={index}>
    //                     <Text>{Element.name}</Text>
    //                     <patch.Textinput placeholder={Element.name}></patch.Textinput>
    //                 </View>
    //         );}
    //     )
    // }
    constructor(props) {
        super(props);
        this.state = {
            name:'',
            phone:'',
            email:'',
            address:'',
            message:'',
            token:'',
            domain:''
        }
    }
    renderTextInput = (dataPlaceholder) => {
        return dataPlaceholder.map( (Element, index) =>{
            return(
                <View key={index}>
                    <Text style={{marginBottom:5}}>{Element.name}</Text>
                    <TextInput style={Styles.textInput} placeholder={Element.name} onChangeText = {(text) => 
                        {index==0
                            ?this.setState({name:text})
                            :index==1?this.setState({phone:text})
                            :index==2?this.setState({email:text})
                        :this.setState({address:text})
                        }
                    } />
                </View>
            );}
        )
    }
    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        const domain = await AsyncStorage.getItem('domain');
        this.setState({token,domain})
    }
    render() {
        console.log(this.state.phone, this.state.name)
        return (
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"THÊM MỚI KHÁCH HÀNG"}/>
                <ScrollView>
                    <View style={styles.content2}>
                        {this.renderTextInput(namePlace)}
                        <Text>Ghi chú</Text>
                        <TextInput style={styles.textarea}
                                   underlineColorAndroid="transparent"
                                   placeholderTextColor="grey"
                                   numberOfLines={3}
                                   multiline={true}/>
                        <TouchableOpacity onPress = {async() => {
                                const {name, phone, email, address, message,token,domain} = this.state;
                                const contact_id = await this.props.navigation.getParam('contact_id');
                                this.props.onClickEditCustomer({token,domain,name,phone,email,address,message,contact_id})}
                            } 
                            style={[styles.button, Styles.buttonSmall]}>
                            <Text style={{color:"white", fontSize: 14}}>Lưu lại</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data : state.EditCustomerReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickEditCustomer: (infor) => {
            dispatch(editCustomerAction(infor));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditCustomer);
const styles = StyleSheet.create({
    content2: {
        paddingLeft : Dimens.padding.md,
        paddingRight : Dimens.padding.md,
        marginTop: Dimens.margin.sm
    },
    inputForm: {
        height : 45,
        borderRadius : 3,
        borderWidth : 1,
        borderColor : "#a6a6a6",
        paddingLeft : Dimens.padding.sm,
        paddingRight : Dimens.padding.sm,
        marginBottom : Dimens.margin.xs,
        marginTop : Dimens.margin.xs,
        color : "#969696",
        backgroundColor : "white",
        fontSize : 13
    },
    textarea: {
        backgroundColor : "white",
        textAlignVertical: "top",
        marginBottom: Dimens.margin.sm,
        marginTop: Dimens.margin.sm,
        padding :Dimens.padding.sm,
        borderRadius : 3,
        borderWidth : 1,
        borderColor : "#a6a6a6",
    },
    button: {
        backgroundColor:"#017EA5",
        alignSelf : "flex-start"
    }
})
