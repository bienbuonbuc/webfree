import React, {Component} from 'react';
import { View, Text } from 'react-native';
import HeaderTabs from '../../header.tabs/HeaderTabs';

export default class HelpCustomer extends Component{
    render(){
        return(
            <View>
                <HeaderTabs back={() => this.props.navigation.goBack()} title={"HỖ TRỢ CHĂM SÓC KHÁCH HÀNG"}></HeaderTabs>
                <View style={{paddingHorizontal: 10, marginTop: 5}}>
                    <Text style={{fontWeight: 'bold', fontSize:16}}>Công ty cổ phần tri thức Vì Dân</Text>
                    <Text>Địa chỉ: số 992, đường Láng, phường Láng Thượng, quận Đống Đa, thành phố Hà Nội</Text>
                    <Text>Hotline: 024 777 04 888</Text>
                    <Text>Chi nhánh: số 98, ngõ 1, đường Lê Văn Thiêm, phường Nhân Chính, quận Thanh Xuân, thành phố Hà Nội</Text>
                </View>
            </View>
        );
    }
}