import { combineReducers } from 'redux';

import LoginReducer from './auth.reducer/login.reducer/login.reducers';
import ManageAccountReducer from './account.reducer/manage.account.reducer/Manage.Account.Reducers';
import NewCustomerReducer from './customer.reducer/new.customer.reducer/NewCustomerReducer';
import InformationCustomerReducer from './customer.reducer/information.customer.reducer/InformationCustomerReducer';
import EditCustomerReducer from './customer.reducer/edit.customer.reducer/EditCustomerReducer';
const Reducers = combineReducers({
    LoginReducer,
    ManageAccountReducer,
    NewCustomerReducer,
    InformationCustomerReducer,
    EditCustomerReducer
});
export default Reducers;