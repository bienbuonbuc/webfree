import *as Types from '../../../constans/action.type';

const EditCustomerReducer = (data = '0', action) => {
    switch(action.type) {
        case Types.FETCH_SUCCESS_EDITCUSTOMER:
            return action.content;
        default:
            return data;
    }

}
export default EditCustomerReducer;