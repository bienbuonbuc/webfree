import *as Types from '../../../constans/action.type';

const NewCustomerReducer = ( data = [], action) => {
    switch(action.type) {
        case Types.FETCH_SUCCESS_NEWCUSTOMER:
            return action.content;
        case Types.FETCH_FAILED_NEWCUSTOMER:
            return data;
        default:
            return data;
    }
}
export default NewCustomerReducer;