import *as Types from '../../../constans/action.type';

const LoginReducer = ( data = '0', action) => {
    switch(action.type) {
        case Types.FETCH_SUCCESS_LOGIN:
            return action.token;
        case Types.FETCH_FAILED_LOGIN:
            return action.error;
        default:
            return data;
    }
}
export default LoginReducer;