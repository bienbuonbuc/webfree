
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import PasswordRetrieval from './src/component/auth/password.retrieval/PasswordRetrieval';
import ManageAccount from './src/component/account/manage.account/ManageAccount';
import AddCustomers from './src/component/customer/add.customers/AddCustomers';
import ButtonLogIn from './src/component/auth/login/ButtonLogIn';
import InformationCustomer from './src/component/customer/information.customer/InformationCustomer';
import EditCustomer from './src/component/customer/edit.customer/EditCustomer';
import EditAccount from "./src/component/account/edit.account/EditAccount";

import MoneyTransfer from './src/component/banking.transactions/money.transfer/MoneyTransfer';
import HistoryTransaction from './src/component/banking.transactions/history.transaction/HistoryTransaction';
import CodeReferral from './src/component/banking.transactions/code.referral/CodeReferral';
import AddInformationBank from './src/component/banking.transactions/add.information.bank/AddInformationBank';
import ManagerAccountBank from './src/component/banking.transactions/manager.account.bank/ManagerAccountBank';
import HelpCustomer from './src/component/customer/help.customer/HelpCustomer';
import InformationAccount from './src/component/account/information.account/InformationAccount';
import AuthAccount from './src/component/account/auth.account/AuthAccount';
import WalletAccount from './src/component/banking.transactions/wallet.account/WalletAccount';
import SecurityAccount from './src/component/account/security.account/SecurityAccount';
import UpgradeWebsite from './src/component/account/upgrade.website/UpgradeWebsite';
import UpgradeEmail from './src/component/account/upgrade.email/UpgradeEmail';
import AddCardAccount from './src/component/banking.transactions/add.card.account/AddCardAccount';
import NewCustomer from './src/component/customer/new.customer/NewCustomer';
import Login from './src/component/auth/login/Login';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { sizeIcon } from './src/styles/values/Dimens';
import React from 'react';
import ManageAccountContainer from './src/containers/account.container/manage.account.container/ManageAccountContainer';
import NewCustomerContainer from './src/containers/customer.container/new.customer.container/NewCustomerContainer';
import PageMoma from './src/component/moma.web/PageMoma';
import Product from './src/component/moma.web/Product';

const customerStack = createStackNavigator({
    NewCustomer: {
        screen: NewCustomerContainer
    },
    InformationCustomer: {
        screen: InformationCustomer
    },
    EditCustomer: {
        screen: EditCustomer
    },
    AddCustomers: {
        screen: AddCustomers
    }
},
{
    defaultNavigationOptions: {
        header: null,
      }
}
);

const walletStack = createStackNavigator({
    WalletAccount: {
        screen: WalletAccount
    },
    InformationAccount: {
        screen: InformationAccount
    },
    ManagerAccountBank: {
        screen: ManagerAccountBank
    },
    MoneyTransfer: {
        screen: MoneyTransfer
    },
    HistoryTransaction: {
        screen: HistoryTransaction
    },
    AddCardAccount: {
        screen: AddCardAccount
    },
    AuthAccount: {
        screen: AuthAccount
    },
    AddInformationBank: {
        screen: AddInformationBank
    }
},
{
    defaultNavigationOptions: {
        header: null,
      }
});

const accountStack = createStackNavigator({
    ManageAccount: {
        screen: ManageAccountContainer
    },
    EditAccount: {
        screen: EditAccount
    },
    CodeReferral: {
        screen: CodeReferral
    },
    UpgradeEmail: {
        screen: UpgradeEmail
    },
    UpgradeWebsite: {
        screen: UpgradeWebsite
    },
    SecurityAccount: {
        screen: SecurityAccount
    },
    HelpCustomer: {
        screen: HelpCustomer
    },
},
{
    defaultNavigationOptions: {
        header: null,
      }
});
const mainBottom = createBottomTabNavigator({
    customerStack: {
        screen: PageMoma,
        navigationOptions: {
            title: "Khách hàng",
            tabBarIcon: ({tintColor}) => (
            <Icon name={"ios-people"} size={30} color={tintColor}/>
            )}
    },
    productStack: {
        screen: Product,
        navigationOptions: {
            title: "Sản phẩm",
            tabBarIcon: ({tintColor}) => (
            <MaterialIcons name={"shopping-cart"} size={30} color={tintColor}/>
            )}
    },
    walletStack: {
        screen: walletStack,
        navigationOptions: {
            title: "Ví của tôi",
            tabBarIcon: ({tintColor}) => (
                <Icon name={"ios-wallet"} size={30} color={tintColor}/>
            )}
    },
    accountStack: {
        screen: accountStack,
        navigationOptions: {
            title: "Tài khoản",
            tabBarIcon: ({tintColor}) => (
                <Icon name={"md-person"} size={30} color={tintColor}/>
            )}
    }
});

const MainStack = createStackNavigator({
    Login: {
        screen: Login
    },
    PasswordRetrieval: {
        screen: PasswordRetrieval
    },
    mainBottom: {
        screen: mainBottom
    }
},
{
    defaultNavigationOptions: {
        header: null,
      }
}
)
export default createAppContainer(MainStack);